
#include "GLControl.h"
#include <glm/gtc/matrix_transform.hpp>

bool GLControl::InitGLEW() {

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	

	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}



	

	// tell GL to only draw onto a pixel if the shape is closer to the viewer
	

	return true;

}

GLControl::GLControl()
{
	m_matrixProjection = glm::mat4(1.0);
	m_matrixOrtho = glm::mat4(1.0);
}

bool GLControl::InitOpenGL()
{
	if (!InitGLEW())
		return false;

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	gl_version = glGetString(GL_VERSION);

	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", gl_version);
	GLlog("renderer: %s\nversion: %s\n", renderer, gl_version);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glShadeModel(GL_SMOOTH);							// Enables Smooth Color Shading
	glEnable(GL_DEPTH_TEST);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	glEnable(GL_BLEND);

	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
	
	return true;
}

void GLControl::SetProjection3D(float fov, float aspectRatio, float near, float far)
{
#define PI  3.14159265358979323846
#define ONE_DEG_IN_RAD (2.0 * PI) / 360.0 

	/*float near = 0.1f; // clipping plane
	float far = 100.0f; // clipping plane
	float fov = 67.0f * ONE_DEG_IN_RAD; // convert 67 degrees to radians
	float aspect = (float)g_gl_width / (float)g_gl_height; // aspect ratio
		*/												   // matrix components
	/*float range = tan(fov * 0.5f) * near;
	float Sx = (2.0f * near) / (range * aspect + range * aspect);
	float Sy = near / range;
	float Sz = -(far + near) / (far - near);
	float Pz = -(2.0f * far * near) / (far - near);*/

	fov *= ONE_DEG_IN_RAD;
	m_matrixProjection = glm::perspective(fov, aspectRatio, near, far);
}

void GLControl::SetOrtho2D(int width, int height)
{
	m_matrixOrtho = glm::ortho(0.0f, float(width), 0.0f, float(height));

}

void GLControl::SetViewport(int width, int height)
{
	m_viewportWidth = width;
	m_viewportHeight = height;
	
}


glm::mat4 * GLControl::GetProjectionMatrix()
{
	return &m_matrixProjection;
}

glm::mat4 * GLControl::GetOrthoMatrix()
{
	return &m_matrixOrtho;
}

int GLControl::GetViewportWidth()
{
	return m_viewportWidth;
}

int GLControl::GetViewportHeight()
{
	return m_viewportHeight;
}


bool RestartGLlog()
{
	FILE* file = fopen(GL_LOG_FILE, "w");
	if (!file) {
		fprintf(
			stderr,
			"ERROR: could not open GL_LOG_FILE log file %s for writing\n",
			GL_LOG_FILE
			);
		return false;
	}
	time_t now = time(NULL);
	char* date = ctime(&now);
	fprintf(file, "GL_LOG_FILE log. local time %s\n", date);
	fclose(file);
	return true;
}

bool GLlog(const char * message, ...)
{
	va_list argptr;

	FILE* file = fopen(GL_LOG_FILE, "a");

	if (!file) {
		fprintf(
			stderr,
			"ERROR: could not open GL_LOG_FILE %s file for appending\n",
			GL_LOG_FILE
			);
		return false;
	}

	va_start(argptr, message);
	vfprintf(file, message, argptr);
	va_end(argptr);
	fclose(file);
	return true;
}

bool GLlogErr(const char * message, ...)
{
	va_list argptr;

	FILE* file = fopen(GL_LOG_FILE, "a");

	if (!file) {
		fprintf(
			stderr,
			"ERROR: could not open GL_LOG_FILE %s file for appending\n",
			GL_LOG_FILE
			);
		return false;
	}
	va_start(argptr, message);
	vfprintf(file, message, argptr);
	va_end(argptr);
	va_start(argptr, message);
	vfprintf(stderr, message, argptr);
	va_end(argptr);
	fclose(file);
	return true;
}

void PrintShaderInfoLog(GLuint shader_index)
{
	
		int max_length = 2048;
		int actual_length = 0;
		char log[2048];
		glGetShaderInfoLog(shader_index, max_length, &actual_length, log);
		printf("shader info log for GL index %i:\n%s\n", shader_index, log);
		GLlog("shader info log for GL index %i:\n%s\n", shader_index, log);
	

}
