
#include "App.h"
#include "Input.h"
#include <iostream>


float App::deltaTime = 0;

void App::DeltaTimeCalc()
{
	double current_seconds = glfwGetTime();
	deltaTime = current_seconds - previous_seconds;
	previous_seconds = current_seconds;

}

bool App::InitializeApp(const char * windowName, int winWidth, int winHeight)
{
	

	GLlog("starting GLFW %s", glfwGetVersionString());



	// start GL context and O/S window using the GLFW helper library
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}

	/* We must specify 3.2 core if on Apple OS X -- other O/S can specify
	anything here. I defined 'APPLE' in the makefile for OS X */

	/*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);*/

#ifdef APPLE
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif



	/*GLFWmonitor* mon = glfwGetPrimaryMonitor ();
	const GLFWvidmode* vmode = glfwGetVideoMode (mon);
	g_window = glfwCreateWindow (
	vmode->width, vmode->height, "Extended GL Init", mon, NULL
	);*/

	m_windowWidth = winWidth;
	m_windowHeight = winHeight;

	window = glfwCreateWindow(m_windowWidth, m_windowHeight, windowName, NULL, NULL	);

	if (!window) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	previous_seconds = 0;
	deltaTime = 0;
	current_scene = nullptr;

	// SETTING CALLBACKS
	glfwSetErrorCallback(App::glfwErrorCallback);
	glfwSetKeyCallback(window, INPUT.KeyboardButtonCallback);
	INPUT.SetWindow(window);
	glfwSetWindowSizeCallback(window, glfwWindowSizeCallback);
	glfwMakeContextCurrent(window);
	// GLFW Options
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glfwWindowHint(GLFW_SAMPLES, 4);

	OPENGLCONTROL.InitOpenGL();
	OPENGLCONTROL.SetViewport(winWidth, winHeight);



	return true;
}

void App::LoadScene(Scene * scene)
{
	current_scene = scene;
	current_scene->Start();

}

void App::MainLoop()
{

	while (!glfwWindowShouldClose(window)) {

		DeltaTimeCalc();
		
		if (current_scene != nullptr) {

			if (current_scene->IsLooping()) {

				
				current_scene->Render();
				glfwPollEvents();
				current_scene->Update();

				glfwSwapBuffers(window);
				
				

			}
			else {

				current_scene->Destroy();
				delete current_scene;
				current_scene = nullptr;

				glfwSetWindowShouldClose(window, 1);

			}
		}
		else
		{

			glfwSetWindowShouldClose(window, 1);

		}
		

		/*


		

		if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_ESCAPE)) {
			glfwSetWindowShouldClose(window, 1);
		}


		glfwSwapBuffers(window);*/
	}

	// close GL context and any other GLFW resources
	Quit();

}

void App::Quit()
{

	glfwTerminate();

}


void App::glfwErrorCallback(int error, const char * description)
{
	fputs(description, stderr);
	GLlogErr("%s\n", description);
}

void App::glfwWindowSizeCallback(GLFWwindow * window, int width, int height)
{
	//m_windowWidth = width;
	//m_windowHeight = height;
	printf("width %i height %i\n", width, height);
	/* update any perspective matrices used here */
	OPENGLCONTROL.SetViewport(width, height);
	OPENGLCONTROL.SetProjection3D(67, (float) width / (float) height, 0.01, 1000);

}

int App::GetWindowWidth()
{
	return m_windowWidth;
}

int App::GetWindowHeight()
{
	return m_windowHeight;
}

