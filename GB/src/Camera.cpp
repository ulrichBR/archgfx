
#include "Camera.h"
#include "App.h"
#include "Input.h"
#include <glm/gtx/rotate_vector.hpp>
#include <glm/matrix.hpp>

const float PI = float(atan(1.0)*4.0);


Camera::Camera()
{
	m_position = glm::vec3(0.0f, 1.0f, 5.0f);
	m_up = glm::vec3(0.0f, 1.0f, 0.0f);
	m_movementSpeed = 25.0f;
	m_mouseSensitivity = 0.1f;
	yaw = -90;
	pitch = 0;
	UpdateCameraVectors();

	m_oldMousePos = new double[2];
	m_oldMousePos = INPUT.GetCursorPosition();
	//m_oldMousePos[0] = 
	//m_oldMousePos[1] = 0;
}


void Camera::SetPosition(glm::vec3 pos)
{
	m_position = pos;

}

void Camera::SetDirection(glm::vec3 dir)
{
	m_direction = dir;

}

void Camera::SetUp(glm::vec3 up)
{
	m_up = up;
}

glm::vec3 Camera::GetPosition()
{
	return m_position;

}

glm::vec3 Camera::GetDirection()
{
	return m_direction;
}

glm::vec3 Camera::GetUp()
{
	return m_up;
}

glm::mat4 Camera::Matrix()
{
	return  glm::lookAt(m_position, m_position + m_direction, m_up);
}

float Camera::GetAngleX()
{
	glm::vec3 vDir = m_direction - m_position;
	vDir = glm::normalize(vDir);
	glm::vec3 vDir2 = vDir; 
	vDir2.y = 0.0f;
	vDir2 = glm::normalize(vDir2);
	float fAngle = acos(glm::dot(vDir2, vDir))*(180.0f / PI);
	if (vDir.y < 0)
		fAngle *= -1.0f;
	return fAngle;
}

void Camera::MouseRotate()
{
	double * currentMousePos = INPUT.GetCursorPosition();

	float deltaX = (float)(currentMousePos[0] - m_oldMousePos[0]);
	float deltaY = (float)(currentMousePos[1] - m_oldMousePos[1]);

	yaw += deltaX * m_mouseSensitivity;
	pitch += deltaY *m_mouseSensitivity;

	// constrainPitch

		if (this->pitch > 89.0f)
			pitch = 89.0f;
		if (this->pitch < -89.0f)
			pitch = -89.0f;

		UpdateCameraVectors();

	//std::cout << currentMousePos[0] << std::endl;

	/*if (deltaX != 0.0f)
	{
		m_direction -= m_position;
		m_direction = glm::rotate(m_direction, deltaX, glm::vec3(0.0f, 1.0f, 0.0f));
		m_direction += m_position;
		
	}
	if (deltaY != 0.0f)
	{
		glm::vec3 vAxis = glm::cross(m_direction - m_position, m_up);

		vAxis = glm::normalize(vAxis);

		float fAngle = deltaY;
		float fNewAngle = fAngle + GetAngleX();

		if (fNewAngle > -89.80f && fNewAngle < 89.80f)
		{
			m_direction -= m_position;
			m_direction = glm::rotate(m_direction, deltaY, vAxis);
			m_direction += m_position;
		}
	}*/

	m_oldMousePos = currentMousePos;

	
}

void Camera::UpdateCameraVectors()
{
	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	m_direction = glm::normalize(front);

}

void Camera::Update()
{
	MouseRotate();

	// Get view direction
	glm::vec3 dirPos = m_position + m_direction;
	glm::vec3 moveVecDir = dirPos - m_position;
	moveVecDir = glm::normalize(moveVecDir);
	moveVecDir *= m_movementSpeed;

	// strafe
	glm::vec3 vStrafe = glm::cross(dirPos - m_position, m_up);
	vStrafe = glm::normalize(vStrafe);
	vStrafe *= m_movementSpeed;

	int iMove = 0;
	glm::vec3 moveBy = glm::vec3(0);

	// Get vector of move
	if (INPUT.GetKey(GLFW_KEY_W)) 
		moveBy += moveVecDir*App::deltaTime;

	if (INPUT.GetKey(GLFW_KEY_S))
		moveBy -= moveVecDir*App::deltaTime;

	if (INPUT.GetKey(GLFW_KEY_A))
		moveBy -= vStrafe*App::deltaTime;

	if (INPUT.GetKey(GLFW_KEY_D))
		moveBy += vStrafe*App::deltaTime;


	m_position += moveBy;

	
	

	
}
