
#include "ModelLoader.h"
#include "Useful.h"
#include <iostream>

ModelLoader::~ModelLoader()
{

}

glm::vec3 ModelLoader::ComputeNormals(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3)
{
	glm::vec3 normal, U, V;

	

	U = point2 - point1;
	V = point3 - point1;

	//cross product
	normal.x = ((U.y * V.z) - (U.z * V.y));
	normal.y = ((U.z * V.x) - (U.x * V.z));
	normal.z = ((U.x * V.y) - (U.y * V.x));

	//modulo
	float lenght = sqrt(normal.x * normal.x + normal.y * normal.y + normal.z * normal.z);

	//evitar divis�o com zero, normaliza
	if (lenght != 0) {
		normal.x = normal.x / lenght;
		normal.y = normal.y / lenght;
		normal.z = normal.z / lenght;
	}

	//if (normal.y < 0) {normal.y *= -1;}
	//if (normal.x < 0) {normal.x *= -1;}
	//if (normal.z < 0) {normal.z *= -1;}

	return normal;

}

glm::vec3 ModelLoader::aiVec3Toglm(aiVector3D point1)
{
	glm::vec3 vec;
	vec.x = point1.x;
	vec.y = point1.y;
	vec.z = point1.z;

	return vec;
}

void ModelLoader::ProcessNode(const aiScene * scene, aiNode * node)
{
	// Processing Mesh
	for (int i = 0; i < node->mNumMeshes; i++) {

		
		bool texCoords = false;

		// Get Mesh
		aiMesh * mesh = scene->mMeshes[node->mMeshes[i]];

		if (mesh->mTextureCoords[0])
			texCoords = true;

		cout << "ModelLoader: Loading Mesh:";


		cout << "\n Normals=" << mesh->HasNormals() << ", TexCoords=" << texCoords << endl;

		// Set to processing
		m_handlingModel->AddMesh(& ProcessMesh(scene, mesh));

	}

	// Recursion in Childreen nodes
	for (int i = 0; i < node->mNumChildren; i++)
	{
		ProcessNode(scene, node->mChildren[i]);
	}

}

Mesh ModelLoader::ProcessMesh(const aiScene * scene, aiMesh * mesh)
{
	
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Material> materials;

	aiVector3D defaultColor(1, 1, 1);
	aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];
	unsigned int dataSize = sizeof(Vertex);
	glm::vec3 normal;

	// Get all Vertices data
	for (int i = 0; i < mesh->mNumVertices; i++) {

		Vertex tmp;
		glm::vec3 tmpVec;

		// Position			
		tmpVec.x = mesh->mVertices[i].x;
		tmpVec.y = mesh->mVertices[i].y;
		tmpVec.z = mesh->mVertices[i].z;
		tmp.position = tmpVec;
		
		// Normals
		if (mesh->HasNormals()) {
			normal = aiVec3Toglm(mesh->mNormals[i]);
		}
		else {
			/*tmpVec.x = 0;
			tmpVec.y = 1;
			tmpVec.z = 0;*/
			if (i % 3 == 0)
				normal = ComputeNormals(aiVec3Toglm(mesh->mVertices[i]), aiVec3Toglm(mesh->mVertices[i + 1]), aiVec3Toglm(mesh->mVertices[i + 2]));
		}

		tmp.normal = normal;

		// Texture Coords
		if (mesh->mTextureCoords[0])
		{
			tmpVec.x = mesh->mTextureCoords[0][i].x;
			tmpVec.y = mesh->mTextureCoords[0][i].y;
		}
		else {
			tmpVec.x = tmpVec.y = tmpVec.z = 1.0;

			
		}

		tmp.texCoords.x = tmpVec.x;
		tmp.texCoords.y = tmpVec.y;

		// Tangent
		/*if (mesh->HasTangentsAndBitangents())
		{
			tmpVec.x = mesh->mTangents[i].x;
			tmpVec.y = mesh->mTangents[i].y;
			tmpVec.z = mesh->mTangents[i].z;
		}
		else {
			tmpVec.x = 1.0;
			tmpVec.y = tmpVec.z = 0;
		}
		tmp.tangent = tmpVec;*/

		// Colors
		/*if (mesh->mColors[0])
		{
			//!= material color
			tmpVec.x = mesh->mColors[0][i].r;
			tmpVec.y = mesh->mColors[0][i].g;
			tmpVec.z = mesh->mColors[0][i].b;
		}
		else {
			tmpVec = defaultColor;
		}
		tmp.color = tmpVec;*/

		

		// Add VBO Data
		//AddVBOData(&tmp, sizeof(VertexData));
		//tempData.insert(tempData.end(), (unsigned char*)(void *)& tmp, (unsigned char*)(void *)& tmp + dataSize);
		vertices.push_back(tmp);
	}

	// Get all Faces data
	for (int i = 0; i <  mesh->mNumFaces; i++) {

		// Current Face
		const aiFace & face = mesh->mFaces[i];
		for (int j = 0;j < face.mNumIndices; j++) //0..2
		{
			indices.push_back(face.mIndices[j]);
		}

	}

	// Get all Texture data
	if (mesh->mMaterialIndex >= 0) {

		const aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		vector<Material> diffuseMaps = LoadMaterials(material,	aiTextureType_DIFFUSE, "texture_diffuse");
		materials.insert(materials.end(), diffuseMaps.begin(), diffuseMaps.end());

		vector<Material> specularMaps = LoadMaterials(material,aiTextureType_SPECULAR, "texture_specular");
		materials.insert(materials.end(), specularMaps.begin(), specularMaps.end());

	}


	return  Mesh( & vertices, &materials,  & indices);
	

	
	//const int vertexTotalSize = sizeof(aiVector3D) * 2 + sizeof(aiVector2D);

	//int totalVertices = 0;

	/*FOR(i, scene->mNumMeshes)
	{
	aiMesh* mesh = scene->mMeshes[i];
	int meshFaces = mesh->mNumFaces;
	m_materialIndices.push_back(mesh->mMaterialIndex);
	int sizeBefore = m_vboModelData.GetCurrentSize();
	m_meshStartIndices.push_back(sizeBefore / vertexTotalSize);

	FOR(j, meshFaces)
	{
	const aiFace & face = mesh->mFaces[j];

	aiVector3D normal;

	if (!mesh->HasNormals()) {
	normal = ComputeNormals(mesh->mVertices[face.mIndices[0]], mesh->mVertices[face.mIndices[1]], mesh->mVertices[face.mIndices[2]]);
	}

	FOR(k, 3)
	{
	aiVector3D pos = mesh->mVertices[face.mIndices[k]];
	aiVector3D uv = mesh->mTextureCoords[0][face.mIndices[k]];

	if (mesh->HasNormals()) {
	normal = mesh->mNormals[face.mIndices[k]];
	}

	m_vboModelData.AddData(&pos, sizeof(aiVector3D));
	m_vboModelData.AddData(&uv, sizeof(aiVector2D));
	m_vboModelData.AddData(&normal, sizeof(aiVector3D));
	}
	}
	int meshVertices = mesh->mNumVertices;
	totalVertices += meshVertices;
	m_meshSizes.push_back((m_vboModelData.GetCurrentSize() - sizeBefore) / vertexTotalSize);
	}

	m_numMaterials = scene->mNumMaterials;

	vector<int> materialRemap(m_numMaterials);


	FOR(i, m_numMaterials)
	{
	const aiMaterial* material = scene->mMaterials[i];
	int a = 5;
	int texIndex = 0;
	aiString path;  // filename

	if (material->GetTexture(aiTextureType_DIFFUSE, texIndex, &path) == AI_SUCCESS)
	{
	string dir = Useful::getFileDirectory(filePath);
	string textureName = path.data;
	string fullPath = dir + textureName;

	int iTexFound = -1;

	for (int j = 0; j < m_textures.size(); j++) {
	if (textureName == m_textures[j]->GetName())
	{
	iTexFound = j;
	break;
	}
	}

	if (iTexFound != -1)
	materialRemap[i] = iTexFound;
	else
	{
	Texture * new_tex = new Texture();
	if (new_tex->LoadFromFile(fullPath, true)) {
	//cout << textureName << endl;
	materialRemap[i] = m_textures.size();
	m_textures.push_back(new_tex);
	}
	}

	}
	}



	FOR(i, m_meshSizes.size())
	{
	int iOldIndex = m_materialIndices[i];
	m_materialIndices[i] = materialRemap[iOldIndex];
	}*/
	
}

vector<Material> ModelLoader::LoadMaterials(const aiMaterial* mat, aiTextureType type, string typeName)
{
	vector<Material> textures;

	for (GLuint i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString textureFileName;
		mat->GetTexture(type, i, &textureFileName);

		bool skip = false;

		for (GLuint j = 0; j < m_loadedTextures.size(); j++)
		{
			if (m_loadedTextures[j].fileName == textureFileName.data)
			{
				textures.push_back(m_loadedTextures[j]);
				skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
				break;
			}
		}

		if (skip == false) {

			Texture * new_tex = nullptr;
			Material mat;


			if (!TXTRMNGR.TextureExists(textureFileName.data)) {

				string dir = Useful::getFileDirectory(tempDirectory);
				string fullPath = dir + textureFileName.data;

				new_tex = new Texture();

				new_tex->LoadFromFile(fullPath, true);


			}
			else {
				new_tex = TXTRMNGR.GetTexture(textureFileName.data);
			}

			mat.textureID = new_tex->GetTextureID();
			mat.type = typeName;
			mat.fileName = textureFileName.data;
			mat.index = index;
			index++;

			textures.push_back(mat);
			m_loadedTextures.push_back(mat);
		}
	}
	return textures;
}

Model * ModelLoader::LoadFromFile(const char * filePath)
{
	Assimp::Importer importer;

	tempDirectory = filePath;

	const aiScene * scene = importer.ReadFile(filePath,
		aiProcess_Triangulate |
		aiProcess_FlipUVs |
		aiProcess_CalcTangentSpace 
		/*
		 |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType*/
		);

	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "ModelLoader: Couldn't load model in " << filePath << "." << std::endl;
		return nullptr;
	}

	//CreateVBO();
	string fileName = Useful::getFileName(filePath);

	if (!ModelExists(fileName)) {

		//if (m_handlingModel != nullptr)
		//	m_handlingModel = nullptr;

		m_handlingModel = new Model();

		ProcessNode(scene, scene->mRootNode);

		m_models.insert(std::pair<std::string, Model*>(fileName, m_handlingModel));


		
	}
	else {

		m_handlingModel = FindModel(fileName);

	}

	//aiReleaseImport(scene);
	importer.FreeScene();
	tempDirectory = "";
	m_loadedTextures.clear();
	index = 0;

	return m_handlingModel;

}

bool ModelLoader::ModelExists(std::string modelName)
{
	std::map<std::string, Model*>::iterator itr = m_models.find(modelName);

	if (itr == m_models.end()) {
		return false;
	}
	else {
		return true;
	}
}

Model * ModelLoader::FindModel(std::string modelName)
{
	if (ModelExists(modelName)) {

		//std::cout << m_textures.find(txte_name)->second->id << " " << txte_name << std::endl;
		return m_models.find(modelName)->second;

	}
	else {

		std::cout << "ModelLoader: Model " << modelName << " was not found." << std::endl;
		return NULL;

	}
}



/*
void ModelLoader::AddVBOData(void * ptrData, unsigned int dataSize)
{
	m_tempData.insert(m_tempData.end(), (unsigned char*)ptrData, (unsigned char*)ptrData + dataSize);
}*/

/*
void ModelLoader::CreateVBO()
{
	// Create VBO
	tempVBO = 0;
	glGenBuffers(1, &tempVBO);
}

void ModelLoader::UploadVBOToGPU(int drawingHint)
{
	// Upload data to GPU
	glBufferData(GL_STATIC_DRAW, m_tempData.size(), &m_tempData[0], drawingHint);
	m_tempData.clear();
}

void ModelLoader::FinalizeMeshLoad()
{
	

	m_tempTextures.clear();
	m_tempData.clear();
	faceIndices.clear();
		// Mesh Creation
	//Mesh * mesh = new Mesh();

}*/