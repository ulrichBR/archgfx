#include "Testing.h"
#include "Shader.h"



void Testing::HelloTriangleInit()
{
	// TRIANGULO HELLO WORLD

	// VBO - vertex buffer object - seta pontos de um objeto
	// cria-se um array de pontos - anti horario
	float points[] = {
		0.0f,  0.5f,  0.0f,
		0.5f, -0.5f,  0.0f,
		-0.5f, -0.5f,  0.0f
	};

	// cria-se um: vertex buffer object (VBO), enviando para a placa grafica
	GLuint vbo = 0;
	glGenBuffers(1, &vbo); // cria um buffer vazio
	glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind - seta um vbo
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points, GL_STATIC_DRAW); // desenha

																			  // VAO -  vertex attribute object - seta atributos dos pontos (textura e coisa e tal)
	vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// FRAGMENT SHADERS - criando fragmento e vertex shaders 
	// VERTEX SHADERS - descreve como os pontos 3D serao apresentados

	/*const char* vertex_shader =
	"#version 400\n"
	"in vec3 vp;"
	"void main () {"
	"  gl_Position = vec4 (vp, 1.0);"
	"}";
	GLuint vs =  glCreateShader (GL_VERTEX_SHADER);
	glShaderSource (vs, 1, &vertex_shader, NULL);
	glCompileShader (vs);*/

	// FRAGMENT SHADERS - descreve as cores da superficie - no caso, do triangulo.
	// apenas serve para descrever as cores.

	/*const char* fragment_shader =
	"#version 400\n"
	"out vec4 frag_colour;"
	"void main () {"
	"  frag_colour = vec4 (0.5, 0.0, 0.5, 1.0);"
	"}";

	GLuint fs = glCreateShader (GL_FRAGMENT_SHADER);
	glShaderSource (fs, 1, &fragment_shader, NULL);
	glCompileShader (fs);*/

	// INICIALIZANDO OS SHADERS - neste caso utiliza-se a classe ShaderUtils

	/*GLuint vs = Shader::loadShaderFromFile("Shaders/test_vs.glsl", GL_VERTEX_SHADER);
	GLuint fs = ShaderUtils::loadShaderFromFile("Shaders/toon.glsl", GL_FRAGMENT_SHADER);



	// anexando os shaders ao programa
	shader_programme = glCreateProgram();
	glAttachShader(shader_programme, fs);
	glAttachShader(shader_programme, vs);
	glLinkProgram(shader_programme);*/
}

void Testing::HelloTriangleLoop()
{

	float lpos[4] = { 1.0,0.0,1.0,0.0 };
	float a = 1;

	// wipe the drawing surface clear
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.6f, 0.6f, 0.8f, 1.0f);

	glLoadIdentity();
	gluLookAt(0.0, 5.0, 5.0,
		0.0, 0.0, 0.0,
		0.0f, 1.0f, 0.0f);

	glLightfv(GL_LIGHT0, GL_POSITION, lpos);
	glRotatef(a, 0, 1, 0);


	glUseProgram(shader_programme);
	glBindVertexArray(vao);
	// draw points 0-3 from the currently bound VAO with current in-use shader
	glDrawArrays(GL_TRIANGLES, 0, 3);
}

void Testing::ToonTestInit()
{



}
