
#include "App.h"
#include "SceneNightVision.h"

int main() {

	App app;



	if (app.InitializeApp("Bom Dia", 800, 600)) {

		app.LoadScene(new SceneNightVision());

		app.MainLoop();

	}

	return 0;
}


