
#include "Mesh.h"
#include "Useful.h"

#include <iostream>
#include <sstream>

Mesh::Mesh()
{

}

Mesh::Mesh(std::vector<Vertex> * vertices, vector<Material> * textures, std::vector<GLuint> * faceIndices)
{

	this->materials = *textures;
	this->indices = *faceIndices;
	this->vertices = *vertices;

	this->Setup();

	// Create VBO
	/*glGenBuffers(1, &m_VBO);
	// Bind VBO and upload to GPU
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, data->size()*sizeof(VertexData), &data->at(0), GL_STATIC_DRAW);


	// Create VAO
	//glGenBuffers(1, &m_VAO);
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_VAO); // Bind VAO
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, faceIndices->size()*sizeof(unsigned int), &faceIndices[0], GL_STATIC_DRAW);

	// Unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);*/


	//unsigned int sizeData = sizeof(VertexData);

	// Vertex positions
	/*glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeData, 0);
	// Normal vectors
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeData, (void*)(3 * sizeof(float)));
	// Tangent Vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeData, (void*)(6 * sizeof(float)));
	// Colors
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeData, (void*)(9 * sizeof(float)));
	// Texture Coordinate
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeData, (void*)(12 * sizeof(float)));
	*/
}

Mesh::~Mesh()
{

}

void Mesh::Setup()
{
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindVertexArray(this->VAO);
	// Load data into vertex buffers
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

	// A great thing about structs is that their memory layout is sequential for all its items.
	// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
	// again translates to 3/2 floats which translates to a byte array.
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex),
		&this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint),
		&this->indices[0], GL_STATIC_DRAW);

	// Set the vertex attribute pointers

	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
		(GLvoid*)0);
	// Vertex Normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, normal));
	// Vertex Texture Coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, texCoords));

	glBindVertexArray(0);

}

void Mesh::Draw(ShaderProgram  shaderProgram)
{
	GLuint diffuseNr = 1;
	GLuint specularNr = 1;
	for (GLuint i = 0; i < this->materials.size(); i++)
	{
		// Activate proper texture unit before binding
										  // Retrieve texture number (the N in diffuse_textureN)
		stringstream ss;
		string number;
		string name = this->materials[i].type;
		if (name == "texture_diffuse") {
			ss << diffuseNr; // Transfer GLuint to stream
			//diffuseNr++;
		}
		else if (name == "texture_specular")
			ss << specularNr++; // Transfer GLuint to stream

		number = ss.str();

		
		GLint loc = glGetUniformLocation(shaderProgram.GetProgramID(), (name + number).c_str());
		glUniform1i(loc, i);

		
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, materials[i].textureID);

	}


	// Draw mesh
	glBindVertexArray(this->VAO);
	
	glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
	//glDrawArrays(GL_TRIANGLES, 0, this->indices.size());

	glBindVertexArray(0);

	// Always good practice to set everything back to defaults once configured.
	for (GLuint i = 0; i < this->materials.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	/*int numMeshes = m_meshSizes.size();

	FOR(i, numMeshes)
	{
		//if (i < m_materialIndices.size()) {
		//	
		//	if (iMatIndex < m_textures.size()) {
		//		
		//	}
		//}
		if (m_textures.size() > 0) {
			int iMatIndex = m_materialIndices[i];
			m_textures[iMatIndex]->BindTexture();
		}
		else {
			//Texture::NoTexture();
		}
		

		glDrawArrays(GL_TRIANGLES, m_meshStartIndices[i], m_meshSizes[i]);
	
		
	
	}*/
}

/*
aiVector3D Mesh::ComputeNormals(aiVector3D point1, aiVector3D point2, aiVector3D point3)
{
	aiVector3D normal, U, V;

	U = point2 - point1;
	V = point3 - point1;

	//cross product
	normal.x = ((U.y * V.z) - (U.z * V.y));
	normal.y = ((U.z * V.x) - (U.x * V.z));
	normal.z = ((U.x * V.y) - (U.y * V.x));

	//modulo
	float lenght = sqrt(normal.x * normal.x + normal.y * normal.y + normal.z * normal.z);

	//evitar divis�o com zero, normaliza
	if (lenght != 0) {
		normal.x = normal.x / lenght;
		normal.y = normal.y / lenght;
		normal.z = normal.z / lenght;
	}

	//if (normal.y < 0) {normal.y *= -1;}
	//if (normal.x < 0) {normal.x *= -1;}
	//if (normal.z < 0) {normal.z *= -1;}

	return normal;

}



void Mesh::RecursiveMeshProcessing(const aiScene * scene, aiNode * node)
{
	// Processing Mesh
	for (int i = 0; i < node->mNumMeshes; i++) {

		// Get Mesh
		aiMesh * mesh = scene->mMeshes[node->mMeshes[i]];

		// Set to processing
		ProcessMesh(scene, mesh);

	}

	// Recursion in Childreen nodes
	for (int i = 0; i < node->mNumChildren; i++)
	{
		RecursiveMeshProcessing(node->mChildren[i], scene);
	}
}

void Mesh::ProcessMesh(const aiScene * scene, aiMesh * mesh)
{

	int numFaces = mesh->mNumFaces;
	int numVertices = mesh->mNumVertices;
	int numMaterials = scene->mNumMaterials;
	std::vector<unsigned int> faceIndices;
	std::vector<VertexData> data;
	glm::vec3 defaultColor(1, 1, 1);
	aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];

	// Get all Vertices data
	for (int i = 0; i < numVertices; i++) {

		VertexData tmp;
		glm::vec3 tmpVec;

		// Position			
		tmpVec.x = mesh->mVertices[i].x;
		tmpVec.y = mesh->mVertices[i].y;
		tmpVec.z = mesh->mVertices[i].z;
		tmp.position = tmpVec;

		// Normals
		if (mesh->HasNormals()) {
			tmpVec.x = mesh->mNormals[i].x;
			tmpVec.y = mesh->mNormals[i].y;
			tmpVec.z = mesh->mNormals[i].z;
		}
		tmp.normal = tmpVec;

		// Tangent
		if (mesh->HasTangentsAndBitangents)
		{
			tmpVec.x = mesh->mTangents[i].x;
			tmpVec.y = mesh->mTangents[i].y;
			tmpVec.z = mesh->mTangents[i].z;
		}
		else {
			tmpVec.x = 1.0;
			tmpVec.y = tmpVec.z = 0;
		}
		tmp.tangent = tmpVec;

		// Colors
		if (mesh->mColors[0])
		{
			//!= material color
			tmpVec.x = mesh->mColors[0][i].r;
			tmpVec.y = mesh->mColors[0][i].g;
			tmpVec.z = mesh->mColors[0][i].b;
		}
		else {
			tmpVec = defaultColor;
		}
		tmp.color = tmpVec;

		// Texture Coords
		if (mesh->HasTextureCoords)
		{
			tmpVec.x = mesh->mTextureCoords[0][i].x;
			tmpVec.y = mesh->mTextureCoords[0][i].y;
		}
		else {
			tmpVec.x = tmpVec.y = tmpVec.z = 0.0;
		}

		tmp.U = tmpVec.x;
		tmp.V = tmpVec.y;
		data.push_back(tmp);
	}

	// Get all Faces data
	for (int i = 0; i < numFaces; i++) {

		// Current Face
		const aiFace & face = mesh->mFaces[i];

		for (int j = 0;j < face.mNumIndices; j++) //0..2
		{
			faceIndices.push_back(face.mIndices[j]);
		}

	}

	// Get all Texture data
	for (int i = 0; i < mat->GetTextureCount(aiTextureType_DIFFUSE); i++)
	{
		aiString fileName;
		const aiMaterial* material = scene->mMaterials[i];

		if (material->GetTexture(aiTextureType_DIFFUSE, i, &fileName) == AI_SUCCESS)
		{
			Texture * new_tex = nullptr;

			if (!TXTRMNGR->TextureExists(fileName.C_Str)) {
				new_tex = new Texture();
				//new_tex->LoadFromFile();
			}
			else {
				new_tex = TXTRMNGR->GetTexture(fileName.C_Str);
			}

			if (!new_tex) {
				m_textures.push_back(new_tex);
			}
		}
	}
	//const int vertexTotalSize = sizeof(aiVector3D) * 2 + sizeof(aiVector2D);

	//int totalVertices = 0;

	FOR(i, scene->mNumMeshes)
	{
	aiMesh* mesh = scene->mMeshes[i];
	int meshFaces = mesh->mNumFaces;
	m_materialIndices.push_back(mesh->mMaterialIndex);
	int sizeBefore = m_vboModelData.GetCurrentSize();
	m_meshStartIndices.push_back(sizeBefore / vertexTotalSize);

	FOR(j, meshFaces)
	{
	const aiFace & face = mesh->mFaces[j];

	aiVector3D normal;

	if (!mesh->HasNormals()) {
	normal = ComputeNormals(mesh->mVertices[face.mIndices[0]], mesh->mVertices[face.mIndices[1]], mesh->mVertices[face.mIndices[2]]);
	}

	FOR(k, 3)
	{
	aiVector3D pos = mesh->mVertices[face.mIndices[k]];
	aiVector3D uv = mesh->mTextureCoords[0][face.mIndices[k]];

	if (mesh->HasNormals()) {
	normal = mesh->mNormals[face.mIndices[k]];
	}

	m_vboModelData.AddData(&pos, sizeof(aiVector3D));
	m_vboModelData.AddData(&uv, sizeof(aiVector2D));
	m_vboModelData.AddData(&normal, sizeof(aiVector3D));
	}
	}
	int meshVertices = mesh->mNumVertices;
	totalVertices += meshVertices;
	m_meshSizes.push_back((m_vboModelData.GetCurrentSize() - sizeBefore) / vertexTotalSize);
	}

	m_numMaterials = scene->mNumMaterials;

	vector<int> materialRemap(m_numMaterials);


	FOR(i, m_numMaterials)
	{
	const aiMaterial* material = scene->mMaterials[i];
	int a = 5;
	int texIndex = 0;
	aiString path;  // filename

	if (material->GetTexture(aiTextureType_DIFFUSE, texIndex, &path) == AI_SUCCESS)
	{
	string dir = Useful::getFileDirectory(filePath);
	string textureName = path.data;
	string fullPath = dir + textureName;

	int iTexFound = -1;

	for (int j = 0; j < m_textures.size(); j++) {
	if (textureName == m_textures[j]->GetName())
	{
	iTexFound = j;
	break;
	}
	}

	if (iTexFound != -1)
	materialRemap[i] = iTexFound;
	else
	{
	Texture * new_tex = new Texture();
	if (new_tex->LoadFromFile(fullPath, true)) {
	//cout << textureName << endl;
	materialRemap[i] = m_textures.size();
	m_textures.push_back(new_tex);
	}
	}

	}
	}



	FOR(i, m_meshSizes.size())
	{
	int iOldIndex = m_materialIndices[i];
	m_materialIndices[i] = materialRemap[iOldIndex];
	}
	





}



void Mesh::FinalizeVBO()
{
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	m_vboModelData.BindVBO();
	m_vboModelData.UploadDataToGPU(GL_STATIC_DRAW);
	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(aiVector3D) + sizeof(aiVector2D), 0);
	// Texture coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(aiVector3D) + sizeof(aiVector2D), (void*)sizeof(aiVector3D));
	// Normal vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(aiVector3D) + sizeof(aiVector2D), (void*)(sizeof(aiVector3D) + sizeof(aiVector2D)));

}

void Mesh::BindModelsVAO()
{

	glBindVertexArray(m_VAO);

}*/