
#include "ShaderProgram.h"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

ShaderProgram::ShaderProgram()
{
	m_linked = false;
	m_numAttributes = 0;

}

/*ShaderProgram::~ShaderProgram()
{
	cout << "shader" << endl;
	//DeleteProgram();
}*/

void ShaderProgram::CreateProgram()
{
	m_programID = glCreateProgram();
}

void ShaderProgram::DeleteProgram()
{

	if (!m_linked)
		return;



	glDeleteProgram(m_programID);
	m_linked = false;

}

bool ShaderProgram::AddShaderToProgram(Shader * shader)
{
	if (!shader->IsLoaded()) {
		std::cout << "ShaderProgram: ShaderID " << shader->GetShaderID() << ": can't be attached in ShaderProgram because it was not loaded." << std::endl;
		return false;
	}

	glAttachShader(m_programID, shader->GetShaderID());

	return true;
}

bool ShaderProgram::LinkProgram()
{
	glLinkProgram(m_programID);
	int iLinkStatus;
	glGetProgramiv(m_programID, GL_LINK_STATUS, &iLinkStatus);
	
	if (iLinkStatus == GL_TRUE)
		m_linked = true;
	else {
		std::cout << "ShaderProgram: ShaderID " << m_programID << ": Impossible to Link." << std::endl;
		PrintShaderInfoLog(m_programID);
	}

	return m_linked;
}

void ShaderProgram::AddAttribute(const std::string attributeName)
{
	glBindAttribLocation(m_programID, m_numAttributes++, attributeName.c_str());

}

GLint ShaderProgram::GetUniformLocation(const std::string & uniformName)
{
	GLint location = glGetUniformLocation(m_programID, uniformName.c_str());
	
	if (location == GL_INVALID_INDEX) {
		cout << "ShaderProgram: ID " << m_programID << endl;
	}

	return location;
}

void ShaderProgram::Use()
{
	if (m_linked) {
		glUseProgram(m_programID);
		//enable all the attributes we added with addAttribute
		for (int i = 0; i < m_numAttributes; i++) {
			glEnableVertexAttribArray(i);
		}
	}
}

void ShaderProgram::Unuse()
{
	glUseProgram(0);
}

GLuint ShaderProgram::GetProgramID()
{
	return m_programID;
}

// Vectors 2 -----------------------------------------------------------------

void ShaderProgram::SetUniform(string parameter, glm::vec2 * vectors, int count)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform2fv(iLoc, count, (GLfloat*)vectors);
}

void ShaderProgram::SetUniform(string parameter, const glm::vec2 vector)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform2fv(iLoc, 1, (GLfloat*)&vector);

}

// Vectors 3 -----------------------------------------------------------------

void ShaderProgram::SetUniform(string parameter, glm::vec3 * vectors, int count)
{

	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform3fv(iLoc, count, (GLfloat*)vectors);

}

void ShaderProgram::SetUniform(string parameter, const glm::vec3 vector)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform3fv(iLoc, 1, (GLfloat*)&vector);
}

// Vectors 4 -----------------------------------------------------------------

void ShaderProgram::SetUniform(string parameter, glm::vec4 * vectors, int count)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform4fv(iLoc, count, (GLfloat*)vectors);


}

void ShaderProgram::SetUniform(string parameter, const glm::vec4 vector)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform4fv(iLoc, 1, (GLfloat*)&vector);

}

// Floats -----------------------------------------------------------------

void ShaderProgram::SetUniform(string parameter, float * values, int count)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform1fv(iLoc, count, values);

}

void ShaderProgram::SetUniform(string parameter, float value)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform1fv(iLoc, 1, &value);


}

// Matrices 3x3 -----------------------------------------------------------------

void ShaderProgram::SetUniform(string parameter, glm::mat3 * matrices, int count)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniformMatrix3fv(iLoc, count, false, (GLfloat*)matrices);

}

void ShaderProgram::SetUniform(string parameter, const glm::mat3 matrix)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniformMatrix3fv(iLoc, 1, false, (GLfloat*)&matrix);

}

// Matrices 4x4 -----------------------------------------------------------------

void ShaderProgram::SetUniform(string parameter, glm::mat4 * matrices, int count)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniformMatrix4fv(iLoc, count, false, (GLfloat*)matrices);
}

void ShaderProgram::SetUniform(string parameter, const glm::mat4 matrix)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniformMatrix4fv(iLoc, 1, GL_FALSE, glm::value_ptr(matrix));
}

// Integers -----------------------------------------------------------------

void ShaderProgram::SetUniform(string parameter, int * values, int count)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform1iv(iLoc, count, values);

}

void ShaderProgram::SetUniform(string parameter, const int value)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform1i(iLoc, value);
}

void ShaderProgram::SetUniform(string parameter, unsigned int value)
{
	int iLoc = glGetUniformLocation(m_programID, parameter.c_str());
	glUniform1ui(iLoc, value);
}

void ShaderProgram::SetModelAndNormalMatrix(string modelMatrixName, string normalMatrixName, glm::mat4 modelMatrix)
{
	SetUniform(modelMatrixName, modelMatrix);
	SetUniform(normalMatrixName, glm::transpose(glm::inverse(modelMatrix)));
}

void ShaderProgram::SetModelAndNormalMatrix(string modelMatrixName, string normalMatrixName, glm::mat4 * modelMatrix)
{
	SetUniform(modelMatrixName, modelMatrix);
	SetUniform(normalMatrixName, glm::transpose(glm::inverse(*modelMatrix)));
}





