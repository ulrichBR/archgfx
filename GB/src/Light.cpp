#include "Light.h"

Light::Light()
{
	m_color = glm::vec3(1.0f, 1.0f, 1.0f);
	SetAngle(glm::vec3(0.0f, -1.0f, 0.0f));
	m_intensity = 1;
	m_type = Directional;
}

Light::Light(glm::vec3 pos, glm::vec3 dir, glm::vec3 color, LightType type)
{
	SetPosition(pos);
	SetAngle(dir);
	m_color = color;
	m_type = type;
}

void Light::SetUniformData(std::string varName)
{
	if (m_shaderProgram != nullptr) {
		m_shaderProgram->SetUniform(varName + ".vColor", m_color);
		m_shaderProgram->SetUniform(varName + ".vDirection", GetAngle());
		m_shaderProgram->SetUniform(varName + ".fAmbient", m_intensity);

	}

}

void Light::SetColor(glm::vec3 color)
{
	m_color = color;
}

void Light::SetType(LightType type)
{
	m_type = type;
}

void Light::SetShaderProgram(ShaderProgram * shaderProgram)
{
	m_shaderProgram = shaderProgram;
}

void Light::SetIntensity(float intensity)
{
	m_intensity = intensity;
}

float Light::GetIntensity()
{
	return m_intensity;
}

glm::vec3 Light::GetColor()
{
	return m_color;
}

LightType Light::GetType()
{
	return m_type;
}

ShaderProgram * Light::GetShaderProgram()
{
	return m_shaderProgram;
}
