#include "Input.h"
#include <iostream>


/*void Input::KeyboardButtonCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	switch (action) {

		//GLFW_KEY_LAST

	case GLFW_PRESS: {
		//INPUT->
		break;
	}

	case GLFW_RELEASE: {

		break;
	}

	case GLFW_REPEAT: {

		break;
	}
	}
}*/

Input::Input()
{
	for (int k = 0; k < GLFW_KEY_LAST; k++) {
		keyLocks[k] = -1;
	}
}

void Input::KeyboardButtonCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
{

	Input::GetInstance().keys[key] = action;
	Input::GetInstance().keyLocks[key] = action;

	std::cout << action << std::endl;
		


}

void Input::SetWindow(GLFWwindow * window)
{
	m_window = window;
}

bool Input::GetKey(int key)
{



	if (m_window != nullptr) {
		
		return glfwGetKey(m_window, key) == GLFW_PRESS;

	} else {

		return false;
	}
}

/*bool Input::GetKeyDown(int key)
{


	if (m_window != nullptr) {

		if (glfwGetKey(m_window, key) == GLFW_PRESS) {
			return true;
		}
		else {
			return false;
		}

	}
	else {

		return false;
	}
}*/

double * Input::GetCursorPosition()
{
	double * pos = new double[2];
	
	glfwGetCursorPos(m_window, &pos[0], &pos[1]);

	return pos;
}

bool Input::GetKeyDown(int keycode)
{
	

	bool keyLock = false;

	if (keyLocks[keycode] == GLFW_REPEAT || keyLocks[keycode] == GLFW_RELEASE || keyLocks[keycode] == -1)
		keyLock = false;
	else 
		keyLock = true;

	bool hit = keys[keycode] && keyLock;

	keyLocks[keycode] = -1;

	return hit;
	

}

bool Input::GetKeyUp(int keycode)
{
	bool keyLock = false;

	//std::cout << keyLocks[keycode] << std::endl;

	if (keyLocks[keycode] == GLFW_REPEAT || keyLocks[keycode] == GLFW_PRESS || keyLocks[keycode] == -1)
		keyLock = false;
	else
		keyLock = true;

	bool hit = !keys[keycode] && keyLock;

	keyLocks[keycode] = -1;

	return hit;
}



