
#include "TextureManager.h"
#include "Useful.h"
#include <FreeImage.h>
#include <iostream>


TextureManager::~TextureManager()
{

	UnloadAllTextures();
	
	

}

Texture * TextureManager::CreateFromData(std::string name,unsigned char * data, int width, int height, int BPP, GLenum format, bool genMipMap)
{

	//BPP = bytes per pixel
	//format = formato de cores
	//genMipMap = gera mip maps

	GLuint gl_texID = m_texturesMap.size()+1;
	GLuint gl_sampler = m_texturesMap.size() + 1;

	//lista de texturas
	glGenTextures(1, &gl_texID);
	glBindTexture(GL_TEXTURE_2D, gl_texID);
	/*glTexEnvi(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // If the u,v coordinates overflow the range 0,1 the image is repeated
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // The magnification function ("linear" produces better results)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);*/

	if (format == GL_RGBA || format == GL_BGRA_EXT) {

		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, data);

		// We must handle this because of internal format parameter
	}
	else if (format == GL_RGB || format == GL_BGR_EXT) {

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, data);

	}
	else if (format == GL_RGBA8) {

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA8, GL_UNSIGNED_BYTE, data);

	}
	else {

		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
	}

	//if (genMipMap) {
		glGenerateMipmap(GL_TEXTURE_2D);
		
	//}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//glTexEnvi(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	/*glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // If the u,v coordinates overflow the range 0,1 the image is repeated
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // The magnification function ("linear" produces better results)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);*/
	GLfloat max_aniso = 0.0f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max_aniso);
	// set the maximum!
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, max_aniso);

	//glGenSamplers(1, &gl_sampler);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	Texture * new_texture = new Texture(gl_texID, gl_sampler, name, width, height, BPP, genMipMap);
	//m_lastTexture = new_texture;
	m_texturesMap.insert(std::pair<std::string, Texture*>(name, new_texture));

	return new_texture;
	

}

Texture * TextureManager::CreateEmptyTexture(std::string name, int width, int height, GLenum format)
{
	GLuint gl_texID = m_texturesMap.size() + 1;
	GLuint gl_sampler = gl_texID;

	glGenTextures(1, &gl_texID);

	glBindTexture(GL_TEXTURE_2D, gl_texID);
	if (format == GL_RGBA || format == GL_BGRA)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
	// We must handle this because of internal format parameter
	else if (format == GL_RGB || format == GL_BGR)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
	else
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);

	//glGenSamplers(1, &gl_sampler);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, 0);

	Texture * new_texture = new Texture(gl_texID, gl_sampler, name, width, height, 32, false);
	m_texturesMap.insert(std::pair<std::string, Texture*>(name, new_texture));

	return new_texture;
}

Texture * TextureManager::LoadFromFile(std::string filePath, bool generateMipMaps)
{
	std::string fileName = Useful::getFileName(filePath);
	

	if(!TextureExists(fileName)) {

		GLuint gl_texID = m_texturesMap.size();

			FREE_IMAGE_FORMAT file_img_fmt = FIF_UNKNOWN;
			FIBITMAP * dib;

		

			// checa o diret�rio do arquivo e deduz o formato
			file_img_fmt = FreeImage_GetFileType(filePath.c_str(), 0);

			// Se for desconhecido, tenta achar o formato do arquivo da extens�o do arquivo
			if (file_img_fmt == FIF_UNKNOWN) {
				file_img_fmt = FreeImage_GetFIFFromFilename(filePath.c_str());
			}

			// � desconhecio, retorna falso
			if (file_img_fmt == FIF_UNKNOWN) {
				std::cout << "TextureManager: Can't load file " << filePath << "." << std::endl;
				return NULL;
			}

			// checa se o plugin � compat�vel e carrega o arquivo
			if (FreeImage_FIFSupportsReading(file_img_fmt)) {
				dib = FreeImage_Load(file_img_fmt, filePath.c_str(), 0);
				if (file_img_fmt == 13) {
					//std::cout << "Image Format is PNG" << std::endl;
				}
			}

			//se a imagem falhou de carregar
			if (!dib) {
				std::cout << "TextureManager: Failed to load " << filePath << "." << std::endl;
				return NULL;
			}

			//if this texture ID is in use, unload the current texture
			if (m_texturesMap.find(fileName) != m_texturesMap.end()) {

				const GLuint value = m_texturesMap[fileName]->GetTextureID();

				glDeleteTextures(1, & value);
				
				m_texturesMap.erase(fileName);
				delete m_texturesMap[fileName];
			}

			// seta os dados da imagem
			FreeImage_FlipVertical(dib);

			//dib = FreeImage_ConvertTo24Bits(dib);
			BYTE* dataPointer = FreeImage_GetBits(dib);

			// se de alguma forma algumas desssa condi��es falharem (n�o deveriam)
			if (dataPointer == NULL || FreeImage_GetWidth(dib) == 0 || FreeImage_GetHeight(dib) == 0) {
				std::cout << "TextureManager: Failed to load " << filePath << ". Width, Height or Data fails." << std::endl;
				return NULL;
			}

			//seta o formato da imagem
			GLenum format = GL_RGBA;
			int bpp = FreeImage_GetBPP(dib);

			if (FreeImage_GetBPP(dib) == 32) { format = GL_RGBA; }
			if (FreeImage_GetBPP(dib) == 24) { format = GL_BGR_EXT; }
			if (FreeImage_GetBPP(dib) == 8) { format = GL_LUMINANCE; }

			

			Texture * newTex = CreateFromData(fileName,dataPointer, FreeImage_GetWidth(dib), FreeImage_GetHeight(dib), FreeImage_GetBPP(dib), format, false);

			FreeImage_Unload(dib);



			return newTex;
	}
	else 
	{
		std::cout << "TextureManager: The file " << filePath << " has been loaded before." << std::endl;
		Texture * newTex = GetTexture(fileName);
		return newTex;
	}

	}

void TextureManager::UnloadTexture(std::string textureName)
{

	if (m_texturesMap.find(textureName) != m_texturesMap.end()) {

		m_texturesMap[textureName]->Delete();		
		delete m_texturesMap.at(textureName);
		m_texturesMap.erase(textureName);



	}

}

void TextureManager::UnloadAllTextures()
{
	for (std::map<std::string, Texture*>::const_iterator it = m_texturesMap.begin(); it != m_texturesMap.end(); it++) {

		it->second->Delete();
		delete it->second;

	}

	m_texturesMap.clear();

	

}

Texture * TextureManager::GetTexture(std::string textureName)
{
	if (TextureExists(textureName)) {

		//std::cout << m_textures.find(txte_name)->second->id << " " << txte_name << std::endl;
		return m_texturesMap.find(textureName)->second;

	}
	else {

		std::cout << "TextureManager: Texture " << textureName << " was not found." << std::endl;
		return NULL;

	}
}



bool TextureManager::TextureExists(std::string textureName)
{
	std::map<std::string, Texture*>::iterator itr = m_texturesMap.find(textureName);

	if (itr == m_texturesMap.end()) {
		return false;
	}
	else {
		return true;
	}
}
 

