
#include "SceneNightVision.h"
#include "GLControl.h"
#include <iostream>
#include "common.h"
#include "App.h"
#include "Input.h"
#include "ModelLoader.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp> 


float Lerp(float start, float end, float percent)
{
	return (start + percent*(end - start));
};

void SceneNightVision::Start()
{
	activeNightVision = false;
	glClearDepth(1.0);
	glClearColor(0, 0, 0.5, 1);
	glEnable(GL_DEPTH_TEST);
	
	shaders[0].LoadFromFile("Shaders\\skybox_vs.glsl", GL_VERTEX_SHADER);
	shaders[1].LoadFromFile("Shaders\\skybox_fs.glsl", GL_FRAGMENT_SHADER);

	shaders[2].LoadFromFile("Shaders\\model_vs.glsl", GL_VERTEX_SHADER);
	shaders[3].LoadFromFile("Shaders\\model_fs.glsl", GL_FRAGMENT_SHADER);

	shaders[4].LoadFromFile("Shaders\\light_vs.glsl", GL_VERTEX_SHADER);
	shaders[5].LoadFromFile("Shaders\\light_fs.glsl", GL_FRAGMENT_SHADER);

	shaders[6].LoadFromFile("Shaders\\screen_vs.glsl", GL_VERTEX_SHADER);
	shaders[7].LoadFromFile("Shaders\\screen_fs.glsl", GL_FRAGMENT_SHADER);
	
	textures[0].LoadFromFile("images\\noise_tex.jpg");
	textures[1].LoadFromFile("images\\google_tex.png");
	textures[0].SetTextureWrapping(TextureWrap::Repeat);


	/*textures[0].LoadFromFile("images\\ambient.png");
	textures[1].LoadFromFile("images\\diffuse.png");
	textures[2].LoadFromFile("images\\specular.png");
	textures[3].LoadFromFile("images\\emission.png");*/
	

	shaderProgramMain.CreateProgram();
	shaderProgramMain.AddShaderToProgram(&shaders[0]);
	shaderProgramMain.AddShaderToProgram(&shaders[1]);
	shaderProgramMain.LinkProgram();

	/*shaderTest.CreateProgram();
	shaderTest.AddShaderToProgram(&shaders[2]);
	shaderTest.AddShaderToProgram(&shaders[3]);
	shaderTest.LinkProgram();*/

	shaderLight.CreateProgram();
	shaderLight.AddShaderToProgram(&shaders[4]);
	shaderLight.AddShaderToProgram(&shaders[5]);
	shaderLight.LinkProgram();
	
	shaderScreen.CreateProgram();
	shaderScreen.AddShaderToProgram(&shaders[6]);
	shaderScreen.AddShaderToProgram(&shaders[7]);
	shaderScreen.LinkProgram();


	//sky.LoadFromFiles("images\\", "elbrus_front.jpg", "elbrus_back.jpg", "elbrus_left.jpg", "elbrus_right.jpg", "elbrus_top.jpg", "elbrus_top.jpg");
	sky.LoadFromFiles("images\\skybox\\", "desert_night_ft.tga", "desert_night_bk.tga", "desert_night_rt.tga", "desert_night_lf.tga", "desert_night_up.tga", "desert_night_dn.tga");
	//model.LoadModel("models\\house\\house.obj");
	model.LoadModel("models\\monkey.obj");
	//model.LoadModel("models\\Wolf\\Wolf.obj");

	objectAngleY = 0;
	noiseEffectTime = 0;
	bufferAmplication = 0;

	OPENGLCONTROL.SetProjection3D(67, 800/600, 0.01f, 1000.f);

	CreateFrameBuffer();
}

void SceneNightVision::Update()
{
	objectAngleY += App::deltaTime * 5.0f;
	

	//if (noiseEffectTime > 1)
	//	noiseEffectTime = 0;

	if (objectAngleY > 360)
		objectAngleY -= 360;

	cam.Update();

	if (INPUT.GetKeyDown(GLFW_KEY_ESCAPE))
		SetLoopig(false);

	if (trigger == 0 || trigger == 3) {
		if (INPUT.GetKeyDown(GLFW_KEY_N)) {

			if (!activeNightVision) {
				bufferAmplication = 0;
				activeNightVision = true;
				trigger = 1;
			}
			else {
				activeNightVision = false;
				trigger = 0;
				timer = 0;
			}

		}
	}
	

		NightVisionAnim();

	
	
		
}

void SceneNightVision::NightVisionAnim()
{
	if (trigger >= 1) {

		// Noise
		noiseEffectTime += App::deltaTime;

		if (noiseEffectTime > 3.14)
			noiseEffectTime = 0;

		if (trigger == 1) {

			timer += App::deltaTime / 0.65;

			if (timer >= 1) {

				timer = 0;
				bufferAmplication = 1;
				noiseAmplification = 6;
				intensityAdjust = 2;
				trigger = 2;
			}
		} else 	if (trigger == 2) {
			// Turning on

			timer += App::deltaTime / 0.85;

			noiseAmplification = Lerp(6, 1, timer);
			intensityAdjust = Lerp(2, 1, timer);

			if (timer >= 1) {
				trigger = 3;
			}
		}

	}

}

void SceneNightVision::Render()
{
	// Bind Framebuffer
	if (activeNightVision)
		glBindFramebuffer(GL_FRAMEBUFFER, FBO);

	// surface clear	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
	glViewport(0, 0, OPENGLCONTROL.GetViewportWidth(), OPENGLCONTROL.GetViewportHeight());
	glEnable(GL_DEPTH_TEST);

	
	
	// Set Shader for Skybox
	shaderProgramMain.Use();

	// Set uniform shader  variables
	glm::mat4 projection = *OPENGLCONTROL.GetProjectionMatrix();
	glm::mat4 view = cam.Matrix();
	glm::mat4 modelMat = glm::mat4(1.0);
	modelMat = glm::rotate(modelMat, 135.0f, glm::vec3(0, 1, 0));
	// Set uniform shader  variables
	shaderProgramMain.SetUniform("projection", projection);
	shaderProgramMain.SetUniform("view", view);
	shaderProgramMain.SetUniform("model", modelMat);
	// Render Skybox
	sky.Render();

	//ShaderProgram::Unuse();

	// Set Shader for Suzanne
	shaderLight.Use();
	shaderLight.SetUniform("projection", projection);
	shaderLight.SetUniform("view", view);
	// Model transformation
	modelMat = glm::translate(glm::mat4(1.0), glm::vec3(0.0, 1, 0.));
	modelMat = glm::rotate(modelMat, objectAngleY, glm::vec3(0, 1, 0));
	shaderLight.SetUniform("model", modelMat);
	// Light uniforms
	shaderLight.SetUniform("lightPos", glm::vec3(0, 0, 10));
	shaderLight.SetUniform("lightColor", glm::vec3(1, 1, 1));
	shaderLight.SetUniform("objectColor", glm::vec3(0.75, 0.5, 0.1));
	
	// Render Suzanne
		model.Draw(shaderLight);

	if (activeNightVision)
		PostProcessing();


	//shaderProgramMain.SetUniform("matrices.modelMatrix", glm::mat4(1.0));
	//shaderProgramMain.SetUniform("matrices.normalMatrix", glm::mat4(1.0));
	//shaderProgramMain.SetUniform("vColor", glm::vec4(1, 1, 1, 1));
	
	//m_dirLight.SetUniformData("sunLight");

	//glm::mat4 T = glm::translate(glm::mat4(1.0f), cam.GetPosition()); // cam translation
	//glm::mat4 R = glm::rotate(glm::mat4(1.0f), (float) 0, glm::vec3(0.0f, 0.0f, 1.0f)); // 
	//glm::mat4 view_mat = R * T;
	//shaderProgramMain.SetUniform("matrices.viewMatrix", view_mat);


	//shaderProgramMain.SetUniform("matrices.modelMatrix", glm::mat4(1.0));

	// Draw the loaded model
	
	//shaderTest.SetUniform("model", glm::mat4(1.0));
	//modelMat = glm::mat4(1.0);
	

	//
	//modelMat = glm::scale(modelMat, glm::vec3(1.f, 1.f, 1.f));	// It's a bit too big for our scene, so scale it down
	//glUniformMatrix4fv(glGetUniformLocation(shaderTest.GetProgramID(), "model"), 1, GL_FALSE, glm::value_ptr(modelMat));
	//shaderProgramMain.SetModelAndNormalMatrix("matrices.modelMatrix", "matrices.normalMatrix", modelMat);

	//shaderTest.Use();
	/*GLint diffuse_map_loc, specular_map_loc, ambient_map_loc, emission_map_loc;
	diffuse_map_loc = glGetUniformLocation(shaderTest.GetProgramID(), "texture_ambient");
	specular_map_loc = glGetUniformLocation(shaderTest.GetProgramID(), "texture_diffuse");
	ambient_map_loc = glGetUniformLocation(shaderTest.GetProgramID(), "texture_specular");
	emission_map_loc = glGetUniformLocation(shaderTest.GetProgramID(), "texture_emission");
	glUniform1i(diffuse_map_loc, 0);
	glUniform1i(specular_map_loc, 1);
	glUniform1i(ambient_map_loc, 2);
	glUniform1i(emission_map_loc, 3);*/


	//shaderTest.Use();   // <-- Don't forget this one!

						// Transformation matrices	
	//shaderTest.SetUniform("projection", projection);
	//shaderTest.SetUniform("view", view);

	/*textures[0].BindTexture(0);
	textures[1].BindTexture(1);
	textures[2].BindTexture(2);
	textures[3].BindTexture(3);*/
	

	
	/*AssimpModel::BindModelsVAO();

	//FOR(i, 7)
	//{

		glm::mat4 mModel = glm::translate(glm::mat4(1.0), glm::vec3(-1, -1, 0));
		//mModel = glm::scale(mModel, glm::vec3(1, 1, 1));

		shaderProgramMain.SetModelAndNormalMatrix("matrices.modelMatrix", "matrices.normalMatrix", mModel);
		//shaderTest.UseProgram();
		model.Render();*/
	//}

//	glm::mat4 mModel = glm::translate(glm::mat4(1.0), glm::vec3(-1, -1, 0));
//	mModel = glm::scale(mModel, glm::vec3(1, 1, 1));
//	shaderProgramMain.SetModelAndNormalMatrix("matrices.modelMatrix", "matrices.normalMatrix", mModel);
	
	

}

void SceneNightVision::Destroy()
{
	shaderProgramMain.DeleteProgram();
	shaderLight.DeleteProgram();
	DeleteFrameBuffer();
	
}



void SceneNightVision::CreateQuadScreen()
{
	GLfloat quadVertices[] = {   // Vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
	  // Positions   // TexCoords
		-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		1.0f, -1.0f,  1.0f, 0.0f,

		-1.0f,  1.0f,  0.0f, 1.0f,
		1.0f, -1.0f,  1.0f, 0.0f,
		1.0f,  1.0f,  1.0f, 1.0f
	};

	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	glBindVertexArray(0);
}

void SceneNightVision::CreateFrameBuffer()
{
	CreateQuadScreen();

	FBO = 0;

	//Create Necessary Textures
	m_renderTexture.CreateEmptyTexture(800, 600, GL_RGB);
	m_depthTexture.CreateEmptyTexture(800, 600, GL_DEPTH_COMPONENT);

	// Generate a FrameBuffer
	glGenFramebuffers(1, &FBO);
	// Bind Framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);

	// Attach textures
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_renderTexture.GetTextureID(), 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthTexture.GetTextureID(), 0);
	
	// Check Framebuffer status
	int i = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	
	if (i != GL_FRAMEBUFFER_COMPLETE)
	{
		string fboError = "GL_FRAMEBUFFER_UNDEFINED";

		if (i == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)
			fboError = "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
		else if (i == GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT)
			fboError = "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
		else if (i == GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER)
			fboError = "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER";
		else if (i == GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER)
			fboError = "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER";
		else if (i == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)
			fboError = "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
		else if (i == GL_FRAMEBUFFER_UNSUPPORTED)
			fboError = "GL_FRAMEBUFFER_UNSUPPORTED";
		else if (i == GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE)
			fboError = "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE";
		else if (i == GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS)
			fboError = "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS";
			

		std::cout << "Framebuffer is not OK, status=" << fboError << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void SceneNightVision::PostProcessing()
{
	////////////////////////////////////////////////////
	// Bind to default framebuffer again and draw the 
	// quad plane with attched screen texture.
	// //////////////////////////////////////////////////
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // Set clear color to white (not really necessery actually, since we won't be able to see behind the quad anyways)
	glClear(GL_COLOR_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST); // We don't care about depth information when rendering a single quad

	// Draw Screen
	shaderScreen.Use();

	
	shaderScreen.SetUniform("screenTexture", 0);
	shaderScreen.SetUniform("noiseTexture", 1);
	shaderScreen.SetUniform("googleTexture", 2);
	shaderScreen.SetUniform("intensityAdjust", intensityAdjust);
	shaderScreen.SetUniform("bufferAmplication", bufferAmplication);
	shaderScreen.SetUniform("noiseAmplification", noiseAmplification);
	shaderScreen.SetUniform("elapsedTime", noiseEffectTime);
	
	glBindVertexArray(quadVAO);
	//glBindTexture(GL_TEXTURE_2D, m_renderTexture.GetTextureID());	// Use the color attachment texture as the texture of the quad plane
	m_renderTexture.BindTexture(0);
	textures[0].BindTexture(1);
	textures[1].BindTexture(2);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}

void SceneNightVision::DeleteFrameBuffer()
{
	glDeleteFramebuffers(1, &FBO);
}
