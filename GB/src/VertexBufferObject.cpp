#include "VertexBufferObject.h"

VertexBufferObject::VertexBufferObject()
{
	m_dataUploaded = false;
	m_bufferID = 0;

}

void VertexBufferObject::CreateVBO(int size)
{
	glGenBuffers(1, &m_bufferID);
	m_data.reserve(size);
	m_size = size;
	m_currentSize = 0;

}

void VertexBufferObject::DeleteVBO()
{
	glDeleteBuffers(1, &m_bufferID);
	m_dataUploaded = false;
	m_data.clear();
}

void * VertexBufferObject::MapBufferToMemory(int usageHint)
{
	if (!m_dataUploaded) 
		return NULL;

	void* ptrRes = glMapBuffer(m_bufferType, usageHint);

	return ptrRes;

}

void * VertexBufferObject::MapSubBufferToMemory(int usageHint, unsigned int offset, unsigned int length)
{
	if (!m_dataUploaded)
		return NULL;

	void* ptrRes = glMapBufferRange(m_bufferType, offset, length, usageHint);

	return ptrRes;

}

void VertexBufferObject::UnmapBuffer()
{
	glUnmapBuffer(m_bufferType);

}

void VertexBufferObject::BindVBO(int bufferType)
{
	m_bufferType = bufferType;
	glBindBuffer(m_bufferType, m_bufferID);

}

void VertexBufferObject::UploadDataToGPU(int drawingHint)
{
	glBufferData(m_bufferType, m_data.size(), &m_data[0], drawingHint);
	m_dataUploaded = true;
	m_data.clear();

}

void VertexBufferObject::AddData(void * ptrData, unsigned int dataSize)
{
	m_data.insert(m_data.end(), (unsigned char*)ptrData, (unsigned char*)ptrData + dataSize);
	m_currentSize += dataSize;

}

void * VertexBufferObject::GetDataPointer()
{
	if (m_dataUploaded)
		return NULL;

	return (void*)m_data[0];
}

unsigned int VertexBufferObject::GetBufferID()
{
	return m_bufferID;
}

int VertexBufferObject::GetCurrentSize()
{
	return m_currentSize;
}


