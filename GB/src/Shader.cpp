
#include "Shader.h";
#include "Useful.h"
#include <fstream>
#include <istream>
#include <sstream>
#include <iostream>


Shader::Shader()
{
	loaded = false;
	shaderID = 0;
	
}

Shader::Shader(string vs_path, GLenum type) {

	LoadFromFile(vs_path,type);
	
	
}


	/*glDetachShader(programID, vertex);
	glDetachShader(programID, fragment);
	glDeleteShader(vertex);
	glDeleteShader(fragment);
	glDeleteProgram(programID);*/
	


/*void Shader::LoadFromFile(std::string vs_path, std::string fs_path)
{
	vertex = Load(vs_path, GL_VERTEX_SHADER);
	fragment = Load(fs_path, GL_FRAGMENT_SHADER);

	programID = glCreateProgram();
	glAttachShader(programID, vertex);
	glAttachShader(programID, fragment);

	glLinkProgram(programID);
	glUseProgram(programID);

}*/

/*bool Shader::GetLinesFromFile(string fileName, bool includePart, std::vector<string>* result)
{
	//ifstream = fopen(fileName.c_str(), "rt");

	ifstream sourceFile(fileName);

	if (!sourceFile.is_open()) {
		printf("Unable to open file %s\n", fileName.c_str());
		return false;
	}

	string directory;
	int slashIndex = -1;

	directory = Useful::getFileDirectory(fileName);

	//cout << directory << endl;


	// Get all lines from a file

	string lineStr;
	bool inIncludePart = false;	

	while (getline(sourceFile,lineStr))
	{
		stringstream ss(lineStr);
		string sFirst;
		ss >> sFirst;

		

		if (sFirst == "#include")
		{
			string sFileName;
			ss >> sFileName;

			if (sFileName.length() > 0 && sFileName[0] == '\"' && sFileName[sFileName.length() - 1] == '\"')
			{
				sFileName = sFileName.substr(1, sFileName.length() - 2);
				GetLinesFromFile(directory + sFileName, true, result);
				
			}
		}
		else if (sFirst == "#include_part")
			inIncludePart = true;
		else if (sFirst == "#definition_part")
			inIncludePart = false;
		else if (sFirst == "#version")
			inIncludePart = false;
		else if (sFirst == "//")
			inIncludePart = false;
		else if (!includePart || (includePart && inIncludePart)) {
			result->push_back(lineStr);
			cout << lineStr << endl;
		}
	}

	sourceFile.close();

	return true;
}
*/

bool Shader::LoadFromFile(string fileName, GLenum type ) {

	 //Open file
	//GLuint shaderID = 0;
	
	
	ifstream sourceFile(fileName.c_str() ); //Source file loaded 

	if (!sourceFile.is_open()) {
		printf("Shader: Unable to open file %s\n", fileName.c_str());
		return false;
	}

	if (sourceFile) { //Get shader source 

		string shaderContent;
		shaderContent.assign((std::istreambuf_iterator< char >(sourceFile)), std::istreambuf_iterator< char >());

		//Create shader ID
		shaderID = glCreateShader(type);
		

		//Set shader source - get pointer of file
		const GLchar* shaderSource = shaderContent.c_str();
		glShaderSource(shaderID, 1, (const GLchar**)&shaderSource, NULL);

		//Compile shader source 
		glCompileShader(shaderID);

		//Check shader for errors 
		GLint shaderCompiled = GL_FALSE;
		glGetShaderiv(shaderID, GL_COMPILE_STATUS, &shaderCompiled);

		if (shaderCompiled != GL_TRUE) {

			printf("Shader: Unable to compile shader %d!\n\nSource:\n%s\n", shaderID, shaderSource);
			PrintShaderInfoLog(shaderID);
			glDeleteShader(shaderID);
			shaderID = 0;
			return false;

		}

		loaded = true;
		shaderType = type;
		return true;
	}
	else {
		printf("Shader: Unable to open file %s\n", fileName.c_str());
		return false;
	}

	/*//Create shader ID
	shaderID = glCreateShader(type);

	//Set shader source  
	glShaderSource(shaderID, lines.size(), shaderSource, NULL); // (const GLchar**) shaderSource

	//Compile shader source 
	glCompileShader(shaderID);
	delete[] shaderSource;

	//Check shader for errors 
	GLint shaderCompiled = GL_FALSE;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &shaderCompiled);

	if (shaderCompiled != GL_TRUE) {
		printf("Unable to compile shader %d!\n\nSource:\n%s\n", shaderID, shaderSource);
		PrintShaderInfoLog(shaderID);
		glDeleteShader(shaderID);
		shaderID = 0;
		return false;
	}
	else {

		shaderType = type;
		loaded = true;

	}

	

	*/

	return true;

	

}

GLuint Shader::GetShaderID()
{
	return shaderID;
}

bool Shader::IsLoaded()
{
	return loaded;
}
