#include "Transform.h"

Transform::Transform()
{
	m_position = glm::vec3(0, 0, 0);
	m_origin = glm::vec3(0, 0, 0);
	m_eulerAngle = glm::vec3(0, 0, 0);
	m_scale = glm::vec3(1, 1, 1);

}

void Transform::SetPosition(glm::vec3 pos)
{
	m_position = pos;

}

void Transform::SetPosition(float x, float y, float z)
{
	m_position.x = x;
	m_position.y = y;
	m_position.z = z;

}

void Transform::SetOrigin(glm::vec3 origin)
{
	m_origin = origin;
}

void Transform::SetOrigin(float x, float y, float z)
{
	m_origin.x = x;
	m_origin.y = y;
	m_origin.z = z;

}

void Transform::SetScale(glm::vec3 scale)
{
	m_scale = scale;

}

void Transform::SetScale(float x, float y, float z)
{
	m_scale.x = x;
	m_scale.y = y;
	m_scale.z = z;


}

void Transform::SetUniformScale(float x)
{
	m_scale = glm::vec3(x, x, x);

}

void Transform::SetAngle(glm::vec3 eulerAngle)
{
	m_eulerAngle = eulerAngle;

}

glm::vec3 Transform::GetOrigin()
{
	return m_origin;
}

glm::vec3 Transform::GetPosition()
{
	return m_position;
}

glm::vec3 Transform::GetScale()
{
	return m_scale;
}

glm::vec3 Transform::GetAngle()
{
	return m_eulerAngle;
}
