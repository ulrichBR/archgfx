
#include "TextureManager.h"
#include <iostream>

Texture::Texture()
{
	m_id = 0;
	m_sampler = 0;
	m_name = "";
	m_width = 1;
	m_height = 1;
	m_BPP = 1;
	m_mipMapsGenerated = false;
	created = false;
}

Texture::Texture(GLuint glTexID, GLuint glSampler, std::string name, int width, int height, int bpp, bool genMipMap)
{
	m_id = glTexID;
	m_sampler = glSampler;
	m_name = name;
	m_width = width;
	m_height = height;
	m_BPP = bpp;	
	m_mipMapsGenerated = genMipMap;
	created = true;

}

bool Texture::CreateEmptyTexture(int width, int height, GLenum format)
{

	if (!created) {
		Texture * tex = TXTRMNGR.CreateEmptyTexture("NewEmptyTexture", width, height, format);

		*this = *tex;
		created = true;

		return true;
	} else {
		std::cout << "Texture: The texture " << m_name << "is in use." << std::endl;
		return false;
	}
	

}

bool Texture::LoadFromFile(std::string fileName, bool genMipMap)
{
	if (!created) {
		Texture * tex = TXTRMNGR.LoadFromFile(fileName, genMipMap);

		if (tex != nullptr) {

			*this = *tex;
			created = true;

			return true;
		}
		else {

			std::cout << "TextureManager: File " + fileName + " not found." << std::endl;
			created = false;

			return false;
		}
	}
	else {
		std::cout << "Texture: The texture " << m_name << "is in use." << std::endl;
		return false;
	}
	
}

void Texture::BindTexture(GLuint textureUnit)
{
	glActiveTexture(GL_TEXTURE0 + textureUnit);
	glBindTexture(GL_TEXTURE_2D, m_id);
	//glBindSampler(textureUnit, m_sampler);

}

void Texture::UnbindTexture(GLuint textureUnit)
{

	glActiveTexture(GL_TEXTURE0 + textureUnit);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::NoTexture()
{

	glBindTexture(GL_TEXTURE_2D, 0);
	
}

void Texture::Delete()
{
	glDeleteSamplers(1, &m_sampler);
	glDeleteTextures(1, &m_id);

}



void Texture::SetFiltering(TextureFiltering magnification, TextureFiltering minification)
{
	//glBindSampler(0, m_sampler);

	// Set magnification filter
	if (magnification == TEXTURE_FILTER_MAG_NEAREST)
		glSamplerParameteri(m_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	else if (magnification == TEXTURE_FILTER_MAG_BILINEAR)
		glSamplerParameteri(m_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Set minification filter
	if (minification == TEXTURE_FILTER_MIN_NEAREST)
		glSamplerParameteri(m_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	else if (minification == TEXTURE_FILTER_MIN_BILINEAR)
		glSamplerParameteri(m_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	else if (minification == TEXTURE_FILTER_MIN_NEAREST_MIPMAP)
		glSamplerParameteri(m_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	else if (minification == TEXTURE_FILTER_MIN_BILINEAR_MIPMAP)
		glSamplerParameteri(m_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	else if (minification == TEXTURE_FILTER_MIN_TRILINEAR)
		glSamplerParameteri(m_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	m_tfMinification = minification;
	m_tfMagnification = magnification;


}

void Texture::SetSamplerParameter(GLenum parameter, GLenum value)
{
	glSamplerParameteri(m_sampler, parameter, value);

}

void Texture::SetTextureWrapping(TextureWrap wrap)
{
	GLint parameter =0;

	if (wrap == TextureWrap::Repeat)
		parameter = GL_REPEAT;
	else if (wrap == TextureWrap::MirroredRepeat)
		parameter = GL_MIRRORED_REPEAT;
	else if (wrap == TextureWrap::ClampToEdge)
		parameter = GL_CLAMP_TO_EDGE;
	else if (wrap == TextureWrap::ClampToBorder)
		parameter = GL_CLAMP_TO_BORDER;
	
	glBindTexture(GL_TEXTURE_2D, m_id);

	

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, parameter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, parameter);
	glBindTexture(GL_TEXTURE_2D, 0);
}

TextureFiltering Texture::GetMinificationFilter()
{
	return m_tfMinification;
}

TextureFiltering Texture::GetMagnificationFilter()
{
	return m_tfMinification;
}

int Texture::GetWidth()
{
	return m_width;
}

int Texture::GetHeight()
{
	return m_height;
}

int Texture::GetBPP()
{
	return m_BPP;
}

GLuint Texture::GetTextureID()
{
	return m_id;
}

GLuint Texture::GetSampler()
{
	return m_sampler;
}

std::string Texture::GetName()
{
	return m_name;
}



