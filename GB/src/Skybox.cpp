
#include "Skybox.h"
#include "TextureManager.h"
#include "common.h"
#include "glm\vec3.hpp"
#include "glm\vec2.hpp"


void Skybox::LoadFromFiles(string filePath, string front, string back, string left, string right, string top, string bottom)
{

	// load skybox textures

	m_texture[0].LoadFromFile(filePath + back);
	m_texture[1].LoadFromFile(filePath + front);
	m_texture[2].LoadFromFile(filePath + right);
	m_texture[3].LoadFromFile(filePath + left);
	m_texture[4].LoadFromFile(filePath + top);
	m_texture[5].LoadFromFile(filePath + bottom);

	FOR(i, 6)
	{
		/*m_texture[i].SetFiltering(TEXTURE_FILTER_MAG_BILINEAR, TEXTURE_FILTER_MIN_BILINEAR);
		m_texture[i].SetSamplerParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		m_texture[i].SetSamplerParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);*/
	}
	

	float size = 200;

	// create vertexes data
	glm::vec3 vSkyBoxVertices[24] =
	{
		// Front face
		glm::vec3(size, size, size), glm::vec3(size, -size, size), glm::vec3(-size, size, size), glm::vec3(-size, -size, size),
		// Back face
		glm::vec3(-size, size, -size), glm::vec3(-size, -size, -size), glm::vec3(size, size, -size), glm::vec3(size, -size, -size),
		// Left face
		glm::vec3(-size, size, size), glm::vec3(-size, -size, size), glm::vec3(-size, size, -size), glm::vec3(-size, -size, -size),
		// Right face
		glm::vec3(size, size, -size), glm::vec3(size, -size, -size), glm::vec3(size, size, size), glm::vec3(size, -size, size),
		// Top face
		glm::vec3(-size, size, -size), glm::vec3(size, size, -size), glm::vec3(-size, size, size), glm::vec3(size, size, size),
		// Bottom face
		glm::vec3(size, -size, -size), glm::vec3(-size, -size, -size), glm::vec3(size, -size, size), glm::vec3(-size, -size, size),
	};

	// create texture coords data
	glm::vec2 vSkyBoxTexCoords[4] =
	{
		//glm::vec2(0.0f, 1.0f), glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), glm::vec2(1.0f, 0.0f)
		glm::vec2(1.0f,0.0f), glm::vec2(1.0f,1.0f), glm::vec2(0.0f, 0.0f), glm::vec2(0.0f, 1.0f)
	};

	// create normals data
	glm::vec3 vSkyBoxNormals[6] =
	{
		glm::vec3(0.0f, 0.0f, -1.0f),
		glm::vec3(0.0f, 0.0f, 1.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(-1.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, -1.0f, 0.0f),
		glm::vec3(0.0f, -1.0f, 0.0f)
	};

	// add on render data
	FOR(i, 24)
	{
		m_vboRenderData.AddData(&vSkyBoxVertices[i], sizeof(glm::vec3));		
		m_vboRenderData.AddData(&vSkyBoxNormals[i / 4], sizeof(glm::vec3));
		m_vboRenderData.AddData(&vSkyBoxTexCoords[i % 4], sizeof(glm::vec2));
	}




	// upload to GPU

	// generate vertex arrays
	m_vboRenderData.CreateVBO();
	m_vboRenderData.BindVBO();

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(this->VAO);
	m_vboRenderData.UploadDataToGPU(GL_STATIC_DRAW);

	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3) + sizeof(glm::vec2), 0);


	// Normal vectors
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3) + sizeof(glm::vec2), (void*)sizeof(glm::vec3));


	// Texture coordinates
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3) + sizeof(glm::vec2), (void*)(sizeof(glm::vec3) + sizeof(glm::vec3)));
	glBindVertexArray(0);
}

void Skybox::Render()
{
	glDepthMask(GL_FALSE);
	glBindVertexArray(VAO);
	FOR(i, 6)
	{
		m_texture[i].BindTexture();
		glDrawArrays(GL_TRIANGLE_STRIP, i * 4, 4);
	}
	glDepthMask(GL_TRUE);
	glBindVertexArray(0);
	FOR(i, 6)
	{
		m_texture[i].UnbindTexture();
	}

}

void Skybox::Delete()
{
	for (int i = 0; i < 6; i++) {

		m_texture[i].Delete();

	}

	glDeleteVertexArrays(1, &VAO);
	m_vboRenderData.DeleteVBO();

}
