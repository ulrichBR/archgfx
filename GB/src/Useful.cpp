
#include <sstream>
#include <iostream>

#include "Useful.h"



std::string Useful::charToString(char * char_array ,int word_size) {

	std::stringstream buffer;

	for (int ch = 0; ch < word_size; ch++) {
			if (char_array[ch] == '\0') {break;}
			buffer << char_array[ch];

	}

	return buffer.str();

}

std::string Useful::getFileFormat(std::string file) {

	std::string name = file;

	name = name.substr(name.length()-4,name.length());

	return name;

}

std::string Useful::getFileDirectory(std::string fname) {

	std::string dir = fname;
	bool found = false;

	for (int i = fname.length() - 1; i >= 0; i--)
	{
		if (i == 0) {
			dir = "";
			break;
		}
		else {
			if (fname[i] == '\\' || fname[i] == '/') {
				dir = fname.substr(0, i + 1);
				break;
			}
		}
	}

	return dir;

}

std::string Useful::getFileName(std::string fname) {
	
	//do diretorório

	
	//std::cout << fname << std::endl;

	//remove o formato
	//name = fname.substr(0,fname.length()-4);

	const size_t last_slash_idx = fname.find_last_of("\\/");
	if (std::string::npos != last_slash_idx)
	{
		fname.erase(0, last_slash_idx + 1);
	}

	//remove o diretório
	/*for (int di = fname.length(); di != 0; di--) {
		if (fname[di] == '\\' ) {
			name = name.substr(di+1,name.length());
			break;
		}
	}*/

	return fname;

}

std::string Useful::readStringIntoQuotes(std::fstream & file) {

			std::stringstream strbuf;
			char cbuf[2];

			file >> cbuf[0];

			if (cbuf[0] == '"') {

				file.get(cbuf[1]);

				while (cbuf[1] != '"') {
				
					if (cbuf[1] != 9) {strbuf << cbuf[1];} //not tabs
					
					file.get(cbuf[1]);					
				
				}
				
			} else {
			
				return "Error! Not quotation marks found!";
			
			}

			return strbuf.str();

}

