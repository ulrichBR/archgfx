
#include "Model.h"
#include "ModelLoader.h"
#include <iostream>

Model::Model()
{
}

Model::Model(string filePath)
{
	LoadModel(filePath);
}

bool Model::LoadModel(std::string filePath)
{

	Model * mdl = MODELLOADER.LoadFromFile(filePath.c_str());

	if (mdl != nullptr) {

		
		
		*this = *mdl;

		std::cout << "ModelLoader: File " + filePath + " was loaded." << std::endl;
		std::cout << "Meshes: " << m_meshes.size() << std::endl;
		//std::cout << "Materials: " << _.size() << std::endl;

		return true;
	}
	else {

		std::cout << "ModelLoader: File " + filePath + " not found." << std::endl;

		return false;
	}

}

void Model::AddMesh(Mesh * mesh)
{
	m_meshes.push_back(*mesh);
}

void Model::Draw(ShaderProgram shaderProgram)
{
	for (int i = 0; i < this->m_meshes.size(); i++)
		this->m_meshes[i].Draw(shaderProgram);

}

