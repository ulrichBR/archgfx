cmake_minimum_required(VERSION 3.0)
project (NightVisionProject)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

# Copy files from source directory to destination directory, substituting any
# variables.  Create destination directory if it does not exist.

macro(configure_files srcDir destDir)
    message(STATUS "Configuring directory ${destDir}")
    make_directory(${destDir})

    file(GLOB templateFiles RELATIVE ${srcDir} ${srcDir}/*)
	
    foreach(templateFile ${templateFiles})
        set(srcTemplatePath ${srcDir}/${templateFile})
		
        if(NOT IS_DIRECTORY ${srcTemplatePath})
		
            message(STATUS "Configuring file ${templateFile}")
            configure_file(
                    ${srcTemplatePath}
                    ${destDir}/${templateFile}
                    @ONLY IMMEDIATE)
					
        endif(NOT IS_DIRECTORY ${srcTemplatePath})
    endforeach(templateFile)
endmacro(configure_files)

macro(copy_dir srcDir destDir)
    file(GLOB_RECURSE templateFiles RELATIVE ${srcDir} ${srcDir}/*)
    foreach(templateFile ${templateFiles})
        message(STATUS "Copying file ${srcDir}/${templateFile}") #...
        #message(STATUS "    ... into ${destDir}/${templateFile}")
        configure_file(${srcDir}/${templateFile} ${destDir}/${templateFile} COPYONLY)
    endforeach(templateFile)
endmacro(copy_dir)




## user defined paths
message("Source Dir: ${PROJECT_SOURCE_DIR}")
message("Destiny Dir: ${CMAKE_BINARY_DIR}")
#SET(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR})

# to add user-defined include path
include_directories(
	../common/include
	include
)
set(CMAKE_PREFIX_PATH "../common")
# set(CMAKE_OSX_ARCHITECTURES i386)

if (NOT WIN32)
set(CMAKE_MODULE_PATH /usr/local/lib/cmake /usr/local/lib/x86_64-linux-gnu/cmake)
set(CMAKE_PREFIX_PATH /usr/local/lib/cmake/glfw)
endif (NOT WIN32)
 
# to add user-defined library path
if (WIN32)
    link_directories(../common/msvc110)
    set(CMAKE_LIBRARY_PATH "../common/msvc110")
endif (WIN32)

if (APPLE)  
   link_directories(../common/osx_64)
   set(CMAKE_LIBRARY_PATH "../common/osx_64")
endif (APPLE)


## required packages
find_package(OpenGL REQUIRED)
if(NOT OPENGL_FOUND)
    message("ERROR: OpenGL not found")
endif(NOT OPENGL_FOUND)

find_package(GLEW REQUIRED)
if(NOT GLEW_FOUND)
    message("ERROR: GLEW not found")
endif(NOT GLEW_FOUND)

#find_package (GLM REQUIRED)
#if(NOT GLM_FOUND)
 #   message("ERROR: GLEW not found")
#endif(NOT GLM_FOUND)

if (APPLE)
find_package(PkgConfig REQUIRED)
pkg_search_module(GLFW REQUIRED glfw3)
if(NOT GLFW_FOUND)
    message("ERROR: GLFW3 not found")
endif(NOT GLFW_FOUND)
endif (APPLE)



#if (WIN32)
	#find_package(GLFW REQUIRED)
	#if (NOT GLFW_FOUND)
	#	message("ERROR: GLFW3 not found")
	#endif (NOT GLFW_FOUND)
#endif (WIN32)

#add_custom_command(TARGET ArchGfxTobias PRE_BUILD
#                   COMMAND ${CMAKE_COMMAND} -E copy_directory
#                   ${CMAKE_SOURCE_DIR}/config $<TARGET_FILE_DIR:ArchGfxTobias>)

file(GLOB NightVision_SRC
    "include/*.h"
    "src/*.cpp"
)


add_executable(NightVision ${NightVision_SRC})

if (WIN32)
add_custom_command(TARGET NightVision POST_BUILD        # Adds a post-build event to MyTest
    COMMAND ${CMAKE_COMMAND} -E copy_directory  # which executes "cmake - E copy_if_different..."
        "${PROJECT_SOURCE_DIR}/../common/DLLs"      # <--this is in-file
        $<TARGET_FILE_DIR:NightVision>)                 # <--this is out-file path
endif (WIN32)	
	#	add_custom_command(TARGET NightVision PRE_BUILD        # Adds a post-build event to MyTest
    #COMMAND ${CMAKE_COMMAND} -E copy_directory  # which executes "cmake - E copy_if_different..."
    #    "${PROJECT_SOURCE_DIR}/Shaders"      # <--this is in-file
    #    $<TARGET_FILE_DIR:NightVision>)                 # <--this is out-file path
	


	
configure_files("${PROJECT_SOURCE_DIR}/Shaders" "${CMAKE_BINARY_DIR}/Shaders")
#copy_files("${PROJECT_SOURCE_DIR}/images" "${CMAKE_BINARY_DIR}")
copy_dir("${PROJECT_SOURCE_DIR}/images" "${CMAKE_BINARY_DIR}/images")
copy_dir("${PROJECT_SOURCE_DIR}/images/skybox" "${CMAKE_BINARY_DIR}/images/skybox")
copy_dir("${PROJECT_SOURCE_DIR}/models" "${CMAKE_BINARY_DIR}/models")
#if (WIN32)
#copy_dir("${PROJECT_SOURCE_DIR}/../common/DLLs" "${CMAKE_BINARY_DIR}")
#endif (WIN32)

target_link_libraries(NightVision  
${OPENGL_LIBRARY} 
${GLEW_LIBRARIES} 
glfw3dll 
FreeImage
${GLFW_LIBRARIES}
assimp)
