/*
	Camera.h
	Created: 06/11/2015
	An OpenGL camera manager, including mouse looking and keyboard control.
	By: Tobias B. Ulrich
*/

#include <glm/vec3.hpp> // glm::vec3
#include <glm/glm.hpp>


#ifndef CAMERA_H
#define CAMERA_H

class Camera {

private:

	// Camera Attributes
	glm::vec3 m_position;
	glm::vec3 m_direction;
	glm::vec3 m_up;


	// Eular Angles
	float yaw;
	float pitch;
	// Camera options
	float m_movementSpeed;
	float m_mouseSensitivity;
	float zoom;

	double * m_oldMousePos;
	
	void MouseRotate();
	void UpdateCameraVectors();

public:


	Camera();


	/// Setters

	void SetPosition(glm::vec3 pos);
	void SetDirection(glm::vec3 dir);
	void SetUp(glm::vec3 up);

	/// Getters

	glm::vec3 GetPosition();
	glm::vec3 GetDirection();
	glm::vec3 GetUp();
	glm::mat4 Matrix();

	// Functions that get viewing angles
	float GetAngleX();
	float GetAngleY();

	void Update();
	void SetMouseLookingSpeed(float speed);
	void SetKeyboardSpeed(float speed);



};

#endif