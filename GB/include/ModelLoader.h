/*

	ModelLoader.h
	Created: 08/11/2015
	Class manager for loading 3D models unsing Assimp.
	By: Tobias B. Ulrich

*/


#include "TextureManager.h"
#include "Model.h"
#include "MeshData.h"

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#ifndef MODELLOADER_H
#define MDEOLLOADER_H

class ModelLoader : public Singleton<ModelLoader> {

	// Model Storage
	std::map<std::string, Model *> m_models;

	// Variable for handle loading model
	Model * m_handlingModel;

	// Variables for handle
	vector<Material> m_loadedTextures;
	int index = 0;
	string tempDirectory;


	// Mesh Loader Functions
	static glm::vec3 ComputeNormals(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3);
	static glm::vec3 aiVec3Toglm(aiVector3D point1);
	void ProcessNode(const aiScene* scene, aiNode * node);
	Mesh ProcessMesh(const aiScene* scene, aiMesh * node);
	vector<Material> ModelLoader::LoadMaterials(const aiMaterial* mat, aiTextureType type, string typeName);


public:

	~ModelLoader();
	Model * LoadFromFile(const char * filePath);
	bool ModelExists(std::string modelName);
	Model * FindModel(std::string modelName);
	


};

#define MODELLOADER ModelLoader::GetInstance()

#endif