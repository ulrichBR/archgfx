/*

	Skybox.h
	Created: 07/11/2015
	Skybox class for OpenGL renderer.
	By: Tobias B. Ulrich

*/

#include "Texture.h"
#include "VertexBufferObject.h"

#ifndef SKYBOX_H
#define SKYBOX_H

using namespace std;

class Skybox {

private:

	Texture m_texture[6];
	VertexBufferObject m_vboRenderData;
	unsigned int VAO;

public:
	

	void LoadFromFiles(string fileName, string front, string back, string left, string right, string top, string bottom);
	void Render();
	void Delete();


};

#endif