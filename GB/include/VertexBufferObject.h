/*

	VertexBufferObject.h
	Vertex Buffer Object storage class for OpenGL.
	By: Michal Bubnar - http://www.mbsoftworks.sk/
	Little modifications by Tobias B. Ulrich

*/

#include <GL\glew.h>
#include <vector>

#ifndef VBO_H
#define VBO_H



class VertexBufferObject
{

private:

	unsigned int m_bufferID;
	int m_size;
	int m_currentSize;
	int m_bufferType;
	std::vector<unsigned char> m_data;

	bool m_dataUploaded;

public:

	/// Constructor

	VertexBufferObject();

	/// Creation

	void CreateVBO(int size = 0);
	void DeleteVBO();

	void * MapBufferToMemory(int usageHint);
	void * MapSubBufferToMemory(int usageHint, unsigned int offset, unsigned int length);
	void UnmapBuffer();

	void BindVBO(int bufferType = GL_ARRAY_BUFFER);
	void UploadDataToGPU(int drawingHint);

	void AddData(void* ptrData, unsigned int uiDataSize);

	void * GetDataPointer();
	unsigned int GetBufferID();
	int GetCurrentSize();

	


};

#endif