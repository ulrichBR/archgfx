/*
	Light.h
	Created: 09/11/2015
	An OpenGL light class.
	By: Tobias B. Ulrich
*/

#include "Transform.h"
#include "ShaderProgram.h"

#ifndef LIGHT_H
#define LIGHT_H

enum LightType {
	Directional = 0,
	Point,
	Spot
};

class Light : public Transform {

private:
	
	LightType m_type;
	glm::vec3 m_color;
	float m_intensity;
	ShaderProgram * m_shaderProgram;

public:

	Light();
	Light(glm::vec3 pos, glm::vec3 dir, glm::vec3 color, LightType type);
	
	void SetUniformData(std::string varName);

	/// Setters

	void SetColor(glm::vec3 color);
	void SetType(LightType type);
	void SetShaderProgram(ShaderProgram * shaderProgram);
	void SetIntensity(float intensity);
	
	/// Getters

	float GetIntensity();
	glm::vec3 GetColor();
	LightType GetType();
	ShaderProgram * GetShaderProgram();



};

#endif