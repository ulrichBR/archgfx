

#include "Scene.h"
#include "ShaderProgram.h"
#include "Camera.h"
#include "Skybox.h"
#include "Model.h"
#include "Light.h"

#ifndef SCENENIGHTVISION_H
#define SCENENIGHTVISION_H

class SceneNightVision : public Scene {

	Shader shaders[8];
	ShaderProgram shaderProgramMain;
	//ShaderProgram shaderTest;
	ShaderProgram shaderLight;
	ShaderProgram shaderScreen;
	Camera cam;
	Skybox sky;
	Model model;
	float objectAngleY;
	float noiseEffectTime;
	Texture textures[4];

	// NightVision Variables
	bool activeNightVision;
	int trigger = 0;
	float timer = 0;
	float intensityAdjust = 1;
	float noiseAmplification = 1;
	float bufferAmplication = 0;

	// Frame buffer variables
	GLuint FBO; 
	GLuint quadVAO, quadVBO;
	Texture m_renderTexture;
	Texture m_depthTexture;


public:

	SceneNightVision() {};

	void Start();
	void Update();
	void Render();
	void Destroy();
	void Reset() {};

	/// Animation Night Vision
	void NightVisionAnim();

	/// Post-Processing Functions

	// Create Quad for screen
	void CreateQuadScreen();
	// Create framebuffer and create empty textures
	void CreateFrameBuffer();
	// Postprocessing function loop
	void PostProcessing();
	// Delete framebuffers
	void DeleteFrameBuffer();



};

#endif