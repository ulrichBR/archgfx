/*
	NonCopyable.hpp
	Created: 13/11/2015
	By: Tobias B. Ulrich
*/

#ifndef NONCOPYABLE_HPP
#define NONCOPYABLE_HPP

class NonCopyable
{
	protected:

	NonCopyable() {}

	private:

	
	NonCopyable(const NonCopyable&);
	NonCopyable& operator =(const NonCopyable&);
};

#endif