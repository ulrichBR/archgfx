/*
	Scene.h
	Created: 06/10/2014
	Update: 08/11/2015
	Base abstract class for scenes used for application.
	Updated to use with glfw3.
	By: Tobias B. Ulrich
*/


#ifndef SCENE_H
#define SCENE_H

class Scene {

protected:

	bool loop;

public:

	Scene();

	//loop module
	void SetLoopig(bool);
	bool IsLooping();

	//class callbacks
	virtual void Start() = 0; // load scene
	virtual void Render() = 0; // renderer callback
	virtual void Update() = 0; // logic for scene
	virtual void Destroy() = 0; // unload scene
	virtual void Reset() {}; // restart scene - reload

};

#endif