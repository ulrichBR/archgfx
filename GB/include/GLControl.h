/*

	GLControl.h
	Created: 08/11/2015
	OpenGL class control. Improves readbility of the code with some utility functions.
	Convenient usage of OpenGL.
	By: Tobias B. Ulrich

*/

#include <stdarg.h> // used by log functions to have variable number of args
#include <stdio.h>
#include <time.h>

#include <GL/glew.h> // include GLEW and new version of GL on Windows
#include <glm/matrix.hpp>

#include "Singleton.hpp"

#ifndef GLCONTROL_H
#define GLCONTROL_H

#define GL_LOG_FILE "gl.log"

/// Log functions

bool RestartGLlog();
bool GLlog(const char* message, ...);
bool GLlogErr(const char* message, ...); /* same as gl_log except also prints to stderr */
void PrintShaderInfoLog(GLuint shader_index);

class GLControl : public Singleton<GLControl> {

private:


	// opengl another stuff
	const GLubyte * gl_version;

	// matrices
	glm::mat4 m_matrixProjection; // Matrix for perspective projection	
	glm::mat4 m_matrixOrtho; // Matrix for orthographic 2D projection

	// Viewport parameters
	int m_viewportWidth;
	int m_viewportHeight;

	bool InitGLEW();

public:

	GLControl();

	/// Glew

	bool InitOpenGL();

	/// Setters
	void SetProjection3D(float fFOV, float fAspectRatio, float fNear, float fFar);
	void SetOrtho2D(int width, int height);
	void SetViewport(int width, int height);

	/// Getters

	glm::mat4 * GetProjectionMatrix();
	glm::mat4 * GetOrthoMatrix();
	int GetViewportWidth();
	int GetViewportHeight();
	


	//void FPSCounter(GLFWwindow* window);
	


};

#define OPENGLCONTROL GLControl::GetInstance()

#endif