/*
	Input.h
	Created: 08/11/2015
	Global Input controller for GLFW3.
	By: Tobias B. Ulrich
*/

#include <GLFW/glfw3.h> 
#include "Singleton.hpp"
#include "NonCopyable.hpp"

#ifndef INPUT_H
#define INPUT_H

class Input : public Singleton<Input>, NonCopyable {



private:

	GLFWwindow * m_window;

	bool keys[GLFW_KEY_LAST];
	int keyLocks[GLFW_KEY_LAST];
	

public:

	Input();

	static void KeyboardButtonCallback(GLFWwindow * window, int key, int scancode, int action, int mods);
	void SetWindow(GLFWwindow * window);
	
	bool GetKey(int key);
	//bool GetKeyDown(int key);
	double * GetCursorPosition();

	// boolean method that returns true if a given key
	// is pressed.
	bool GetKeyDown(int keycode);

	// boolean method that returns true if a given key
	// is pressed.
	bool GetKeyUp(int keycode);
};

#define INPUT Input::GetInstance()

#endif