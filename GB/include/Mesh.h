/*

	Mesh.h
	Created: 08/11/2015
	Storage class for 3D meshes.
	By: Tobias B. Ulrich

*/

#include "ShaderProgram.h"
#include "MeshData.h"

#include <vector>

#ifndef MESH_H
#define MESH_H

class Mesh {

private:

	vector<Vertex> vertices;
	vector<GLuint> indices;
	vector<Material> materials;

	GLuint VAO;
	GLuint VBO;
	GLuint EBO;


	//vector<int> m_meshStartIndices;
	//vector<int> m_meshSizes;
	//vector<int> m_materialIndices;

public:

	Mesh();
	Mesh(std::vector<Vertex> * vertices,vector<Material> * textures, std::vector<GLuint> * faceIndices);
	~Mesh();
	void Setup();
	void Draw(ShaderProgram shaderProgram);
	


	

};

#endif