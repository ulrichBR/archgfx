/*

	Shader.h
	Created: 08/08/2015
	Update: 06/11/2015
	Storage class for OpenGL shaders.
	By: Tobias B. Ulrich

*/



#include "GLControl.h"
#include <string>
#include <vector>

using namespace std;

#ifndef SHADER_H
#define SHADER_H

class Shader {

private:

	bool loaded;
	GLuint shaderID;
	GLenum shaderType;

	// Extension function for #includes in GLSL
	//bool GetLinesFromFile(string fileName, bool includePart, std::vector<string> * result);
	

public:

	

	// Constructor for shader program.
	Shader();
	Shader(std::string path, GLenum shaderType);
	
	// Static function for loading shader files (.glsl) 
	bool LoadFromFile(std::string path, GLenum shaderType);

	// Get shader program ID.
	GLuint GetShaderID();

	bool IsLoaded();

};

#endif