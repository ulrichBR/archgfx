
#include <glm\vec3.hpp>
#include <string>

#ifndef MESHDATA_H
#define MESHDATA_H

struct Vertex {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoords;
	
	//glm::vec3 color;
	//glm::vec3 tangent;

};

struct Material {

	unsigned int textureID;
	std::string type;
	std::string fileName;
	int index;
	
};



#endif