/*

	TextureManager.h
	Created: 05/10/2014
	Update: 07/11/2015
	Manager for loading textures in OpenGL using FreeImage lib (3.17.0)
	Updated for new OpenGL versions and using new class Texture.
	By: Tobias B. Ulrich

*/

#include <string>
#include <map>
#include "Texture.h"
#include "Singleton.hpp"


#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H


class TextureManager : public Singleton<TextureManager>
{

private:

	std::map<std::string, Texture*> m_texturesMap;

public:

	/// Constructor and Destructor
	TextureManager() {};
	~TextureManager();

	/// Create and load functions

	Texture * CreateFromData(std::string name,unsigned char * data, int width, int height, int BPP, GLenum format, bool genMipMap);
	Texture * CreateEmptyTexture(std::string name, int width, int height, GLenum format);
	Texture * LoadFromFile(std::string filePath, bool generateMipMaps = false);
	//bool CreateCubeMap(std::string frontFile, std::string backFile, std::string topFile, std::string bottomFile, std::string leftFile, std::string rightFile);

	/// Unload functions

	void UnloadTexture(std::string textureName);
	void UnloadAllTextures();

	/// Getters and others

	Texture * GetTexture(std::string textureName);
	bool TextureExists(std::string textureName);

	

};


#define TXTRMNGR TextureManager::GetInstance()

#endif
