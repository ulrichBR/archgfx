/*

	Model.h
	Created: 08/11/2015
	Class for 3D models.
	By: Tobias B. Ulrich

*/


#include "Mesh.h"
#include "ShaderProgram.h"
#include <string>

#ifndef MODEL_H
#define MODEL_H

class Model {



private:

	vector<Mesh> m_meshes;

public:

	Model();
	Model(string filePath);
	bool LoadModel(std::string filePath);
	void AddMesh(Mesh * mesh);
	void Draw(ShaderProgram shaderProgram);

};


#endif