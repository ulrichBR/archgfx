/*
	Texture.h
	Created: 07/11/2015
	An OpenGL texture storage class.
	By: Tobias B. Ulrich
*/

#include <string>
#include <GL/glew.h>



#ifndef TEXTURE_H
#define TEXTURE_H

enum TextureFiltering
{
	TEXTURE_FILTER_MAG_NEAREST = 0, // Nearest criterion for magnification
	TEXTURE_FILTER_MAG_BILINEAR, // Bilinear criterion for magnification
	TEXTURE_FILTER_MIN_NEAREST, // Nearest criterion for minification
	TEXTURE_FILTER_MIN_BILINEAR, // Bilinear criterion for minification
	TEXTURE_FILTER_MIN_NEAREST_MIPMAP, // Nearest criterion for minification, but on closest mipmap
	TEXTURE_FILTER_MIN_BILINEAR_MIPMAP, // Bilinear criterion for minification, but on closest mipmap
	TEXTURE_FILTER_MIN_TRILINEAR, // Bilinear criterion for minification on two closest mipmaps, then averaged
};

enum TextureWrap
{
	Repeat,
	MirroredRepeat,
	ClampToEdge,
	ClampToBorder
	
};




class Texture {

private:

	
	std::string m_name;
	GLuint m_id; // opengl id
	GLuint m_sampler; // opengl sampler id
	int m_width;
	int m_height;
	int m_BPP; // bytes per pixel
	bool m_mipMapsGenerated; // mip maps
	TextureFiltering m_tfMinification; // min texture filter
	TextureFiltering m_tfMagnification; // max texture filter
	bool created;

public:

	Texture();
	Texture(GLuint glTexID, GLuint sampler, std::string name, int width, int height, int bpp, bool genMipMap);

	/// Functions

	bool CreateEmptyTexture(int width, int height, GLenum format = GL_RGBA8);
	bool LoadFromFile(std::string fileName, bool genMipMap = false);
	// Binding texture
	void BindTexture(GLuint = 0);
	// Binding texture
	static void UnbindTexture(GLuint = 0);
	// No bind tex
	static void NoTexture();
	// Delete texture buffers
	void Delete();


	/// Setters

	void SetFiltering(TextureFiltering magnification, TextureFiltering minification);
	void SetSamplerParameter(GLenum parameter, GLenum value);
	void SetTextureWrapping(TextureWrap wrap);

	/// Getters

	TextureFiltering GetMinificationFilter();
	TextureFiltering GetMagnificationFilter();
	int GetWidth();
	int GetHeight();
	int GetBPP();
	GLuint GetTextureID();
	GLuint GetSampler();
	std::string GetName();

	/// Operators
	
	//Texture & operator =(const Texture &);

};


#endif