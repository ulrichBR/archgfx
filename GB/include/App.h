/*

	App.h
	Created: 08/11/2015
	Main class for window (GLFW3) handling and comunication with OpenGLControl class.
	By: Tobias B. Ulrich

*/

#include "Scene.h"
#include "GLControl.h"
#include <GLFW/glfw3.h> // GLFW helper library


#ifndef APP_H
#define APP_H



class App {

private:



	// Scenes handle
	Scene * current_scene;

	// window handle
	GLFWwindow * window;

	// window sizes
	int m_windowWidth;
	int m_windowHeight;

	// timer
	double previous_seconds;
   

	void DeltaTimeCalc();

public:

	bool InitializeApp(const char * windowName, int winWidth, int winHeight);
	void LoadScene(Scene *);
	void MainLoop();
	void Quit();
	int GetWindowWidth();
	int GetWindowHeight();

	static void glfwErrorCallback(int error, const char* description);
	static void glfwWindowSizeCallback(GLFWwindow * window, int width, int height);

	static float deltaTime;

};



#endif