/*
	ShaderProgram.h
	Created: 08/10/2015
	Class for OpenGL Shader Programs.
	By: Tobias B. Ulrich
*/

#include "Shader.h"

#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

class ShaderProgram {

private:

	GLuint m_programID; // ID of program
	bool m_linked; // Whether program was linked and is ready to use
	int m_numAttributes;

public:

	ShaderProgram();
	//~ShaderProgram();

	void CreateProgram();
	void DeleteProgram();

	bool AddShaderToProgram(Shader * shShader);
	bool LinkProgram();

	void AddAttribute(const std::string attributeName);
	GLint GetUniformLocation(const std::string& uniformName);

	/// Getters

	GLuint GetProgramID();

	// Use shader program function.
	void Use();

	// Set to zero shader program.
	static void Unuse();

	

	// Setting vectors
	void SetUniform(string parameter, glm::vec2* vectors, int count = 1);
	void SetUniform(string parameter, const glm::vec2 vector);
	void SetUniform(string parameter, glm::vec3* vectors, int count = 1);
	void SetUniform(string parameter, const glm::vec3 vector);
	void SetUniform(string parameter, glm::vec4* vectors, int count = 1);
	void SetUniform(string parameter, const glm::vec4 vector);

	// Setting floats
	void SetUniform(string parameter, float* values, int count = 1);
	void SetUniform(string parameter, float value);

	// Setting 3x3 matrices
	void SetUniform(string parameter, glm::mat3* matrices, int count = 1);
	void SetUniform(string parameter, const glm::mat3 matrix);

	// Setting 4x4 matrices
	void SetUniform(string parameter, glm::mat4* matrices, int count = 1);
	void SetUniform(string parameter, const glm::mat4 matrix);

	// Setting integers
	void SetUniform(string parameter, int* values, int count = 1);
	void SetUniform(string parameter, const int value);
	void SetUniform(string parameter, unsigned int value);

	// Model and normal matrix setting ispretty common
	void SetModelAndNormalMatrix(string modelMatrixName, string normalMatrixName, glm::mat4 modelMatrix);
	void SetModelAndNormalMatrix(string modelMatrixName, string normalMatrixName, glm::mat4* modelMatrix);

	

};

#endif