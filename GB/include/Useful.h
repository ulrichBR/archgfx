/*
	UsefulFunctions.h
	Created: 26/11/2013
	Update: 16/04/2014
	New Update: 07/11/2015
	Useful functions using inline
	By: Tobias B. Ulrich
*/


#if !defined(_USEFUL_H)
#define _USEFUL_H

#include <string>
#include <fstream>


namespace Useful {

	std::string charToString(char*, int);

	std::string readStringIntoQuotes(std::fstream &);

	std::string getFileDirectory(std::string);

	std::string getFileName(std::string);

	std::string getFileFormat(std::string);

}




#endif