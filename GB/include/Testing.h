#pragma once

#include <GL/glew.h>
#include <glm\glm.hpp>
#include <glm\vec3.hpp>

class Testing {

private:
	GLuint shader_programme;
	GLuint vao;

public:
	void HelloTriangleInit();
	void HelloTriangleLoop();
	void ToonTestInit();
	void ToonLoop();

};