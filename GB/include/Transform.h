/*

	Transform.h
	Created: 06/10/2014
	Update: 07/11/2015
	Base class for OpenGL transform. Update: using glm.
	By: Tobias B. Ulrich

*/

#include <glm/vec3.hpp>

#ifndef TRANSFORM_H
#define TRANSFORM_H

class Transform {

private:

	glm::vec3 m_position;
	glm::vec3 m_origin;
	glm::vec3 m_scale;
	glm::vec3 m_eulerAngle;


public:

	Transform();

	// Setters

	void SetPosition(glm::vec3 pos);
	void SetPosition(float x, float y, float z);
	void SetOrigin(glm::vec3 origin);
	void SetOrigin(float x, float y, float z);
	void SetScale(glm::vec3 scale);
	void SetScale(float x, float y, float z);
	void SetUniformScale(float x);
	void SetAngleX(float x);
	void SetAngleY(float y);
	void SetAngleZ(float y);
	void SetAngle(glm::vec3 eulerAngle);

	// Getters

	glm::vec3 GetOrigin();
	glm::vec3 GetPosition();
	glm::vec3 GetScale();
	glm::vec3 GetAngle();

};


#endif