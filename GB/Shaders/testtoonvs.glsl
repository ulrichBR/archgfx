
layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec3 vertex_normal;
uniform mat4 projection_mat, view_mat, model_mat;

varying vec3 vNormal;
varying vec3 vVertex;

 //out vec3 position_eye, normal_eye;

void main(void)
{
 vVertex =  vec3 (view_mat * model_mat * vec4 (vertex_position, 1.0));//gl_Vertex.xyz;
 vNormal = vec3 (view_mat * model_mat * vec4 (vertex_normal, 0.0)); //gl_Normal;
 
 gl_Position = projection_mat * vec4 (vVertex, 1.0);
 //gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

}