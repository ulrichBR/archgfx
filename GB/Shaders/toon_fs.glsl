#version 410

in vec3 position_eye, normal_eye;

uniform mat4 view_mat;

// fixed point light properties
vec3 light_position_world  = vec3 (0.0, 0.0, 2.0);
vec3 Ls = vec3 (0.5, 0.5, 1.0); // white specular colour
vec3 Ld = vec3 (0.7, 0.7, 0.7); // dull white diffuse light colour
vec3 La = vec3 (0.2, 0.2, 0.2); // grey ambient colour
  
// surface reflectance
vec3 Ks = vec3 (1.0, 1.0, 1.0); // fully reflect specular light
vec3 Kd = vec3 (0.5, 0.4, 1.0); // orange diffuse surface reflectance
vec3 Ka = vec3 (1.0, 1.0, 1.0); // fully reflect ambient light
float specular_exponent = 100.0; // specular 'power'

out vec4 fragment_colour; // final colour of surface

#define shininess 20.0

// calculate diffuse component of lighting
float diffuseSimple(vec3 L, vec3 N){
   return clamp(dot(L,N),0.0,1.0);
}

// calculate specular component of lighting
float specularSimple(vec3 L,vec3 N,vec3 H){
   if(dot(N,L)>0){
      return pow(clamp(dot(H,N),0.0,1.0),64.0);
   }
   return 0.0;
}

void main () {
	// ambient intensity
	vec3 Ia = La * Ka;

	// diffuse intensity
	// raise light position to eye space
	vec3 light_position_eye = vec3 (view_mat * vec4 (light_position_world, 1.0));
	vec3 distance_to_light_eye = light_position_eye - position_eye;
	vec3 direction_to_light_eye = normalize (distance_to_light_eye);
	float dot_prod = dot (direction_to_light_eye, normal_eye);
	dot_prod = max (dot_prod, 0.0);
	vec3 Id = Ld * Kd * dot_prod; // final diffuse intensity

	
	// specular intensity
	vec3 surface_to_viewer_eye = normalize (-position_eye);
	
	//vec3 reflection_eye = reflect (-direction_to_light_eye, normal_eye);
	//float dot_prod_specular = dot (reflection_eye, surface_to_viewer_eye);
	//dot_prod_specular = max (dot_prod_specular, 0.0);
	//float specular_factor = pow (dot_prod_specular, specular_exponent);
	
	// blinn
	vec3 half_way_eye = normalize (surface_to_viewer_eye + direction_to_light_eye);
	//float dot_prod_specular = max (dot (half_way_eye, normal_eye), 0.0);
	//float specular_factor = pow (dot_prod_specular, specular_exponent);
	
	//vec3 Is = Ls * Ks * specular_factor; // final specular intensity
	
	//float metallic = dot(normal_eye, position_eye); // cos of angle between N and V
	// if cos > 0.6 - full intensity
	// if cos < 0.4 - low intensity
	// if cos is within [0.4, 0.6] - interpolate values
	//metallic = smoothstep(0.4,0.6,metallic); // smooth interpolation between values
	// shift matallic value from range [0, 1] to range[0.5, 1]
	//metallic = metallic/2 + 0.5; // shift metallic intensity by 0.5
	//fragment_colour *= metallic * Kd;//u_baseColor; // modulate final color
	
	// final colour
	//fragment_colour = vec4 (Is + Id + Ia, 1.0);
	
	
	 
   float iambi = 0.1;
   float idiff = diffuseSimple(direction_to_light_eye, normal_eye);
   float ispec = specularSimple(direction_to_light_eye,normal_eye, half_way_eye);
   float intensity = iambi + idiff + ispec;

	
	 // quantize intensity for cel shading
	 float u_numShades = 4;
   float shadeIntensity = ceil(intensity * u_numShades)/ u_numShades;

   // use base color
   fragment_colour.xyz = Kd*shadeIntensity ;
   // or use color from texture
   //fragment_colour.xyz = Ls*shadeIntensity ;
   // or use mixed colors
   //fragment_colour.xyz = Ls *shadeIntensity ;

   fragment_colour.w = 1.0;
	


	
	
}
