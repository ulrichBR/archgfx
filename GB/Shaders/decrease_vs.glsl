//////////////////////////////////
// VERTEX SHADER /////////////////
#version 330

uniform mat4 projection_mat, view_mat, model_mat;

// vertex attributes 
layout(location = 0) in vec3 i_position;
layout(location = 1) in vec3 i_normal;
//layout(location = 2) in vec2 i_texcoord1;

uniform mat4 u_viewProj_mat; // view-projection matrix
uniform mat4 u_model_mat; // model matrix
uniform mat3 u_normal_mat; // normal matrix

uniform vec3 u_light_position;
uniform vec3 u_camera_position;

// inputs for fragment shader
out vec3 v_normal;
out vec2 v_texcoord1;
out vec3 v_directionToLight;
out vec3 v_directionToCamera;

void main(void){
   vec4 worldPos = u_model_mat * vec4(i_position, 1.0);
   v_normal = vec3 (view_mat * model_mat * vec4 (i_normal, 0.0)); //gl_Normal;
   //v_normal = u_normal_mat * i_normal;
   //v_texcoord1 = i_texcoord1;

   vec3 vectorToLight = u_light_position - worldPos.xyz;
   v_directionToLight = normalize( vectorToLight);
   v_directionToCamera = normalize( u_camera_position - worldPos.xyz );

   vec3 v_vertex =  vec3 (view_mat * model_mat * vec4 (i_position, 1.0));//gl_Vertex.xyz;
  
   gl_Position = projection_mat * vec4 (v_vertex, 1.0);
   //gl_Position = u_viewProj_mat * worldPos;
}
