
#version 400

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;


out vec3 norm_eye;
out vec3 pos_eye;
out vec2 TexCoords;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;



void main()
{
	TexCoords = texCoords;
	norm_eye = (view * vec4 (normal, 0.0)).xyz;
	pos_eye = (view * vec4 (position, 1.0)).xyz;
    gl_Position = projection * vec4 (pos_eye, 1.0);
	//gl_Position = projection * view * model * vec4(position, 1.0f);

    
}