#version 400

in vec2 TexCoords;

out vec4 outputColor;

uniform vec4 color;
uniform sampler2D texture_diffuse1;

void main()
{    

	vec4 texColor = texture(texture_diffuse1, TexCoords);
	outputColor =  texColor;
 
}
