#version 400


in vec3 pos_eye;
in vec3 norm_eye;
in vec2 TexCoords;

uniform mat4 view;

uniform sampler2D texture_diffuse;
uniform sampler2D texture_ambient;
uniform sampler2D texture_specular;
uniform sampler2D texture_emission;

out vec4 outputColor;

vec3 light_position_world = vec3 (1.0, 1.0, 10.0);
vec3 Ls = vec3 (1.0, 1.0, 1.0); // white specular colour
vec3 Ld = vec3 (0.7, 0.7, 0.7); // dull white diffuse light colour
vec3 La = vec3 (0.2, 0.2, 0.2); // grey ambient colour
float specular_exponent = 100.0; // specular 'power'

void main()
{    

	
	vec3 light_pos_eye = (view * vec4 (light_position_world, 1.0)).xyz;
	
	// cor ambiente
	vec3 Ka = texture (texture_ambient, TexCoords).rgb;
	vec3 Ia = vec3 (0.5, 0.5, 0.5) * Ka;

	// difusse
	vec4 texel = texture (texture_diffuse, TexCoords);
	vec3 Kd = texel.rgb;
	vec3 surface_to_light_eye = normalize (light_pos_eye - pos_eye);
	float dp = max (0.0, dot (norm_eye, surface_to_light_eye));
	vec3 Id = Kd * Ld * dp; 

	// specular
	vec3 Ks = texture (texture_specular, TexCoords).rgb;
	vec3 surface_to_viewer_eye = normalize (-pos_eye);
	vec3 half_way_eye = normalize (surface_to_viewer_eye + surface_to_light_eye);
	float dot_prod_specular = max (dot (half_way_eye, norm_eye), 0.0);
	float specular_factor = pow (dot_prod_specular, specular_exponent);
	vec3 Is = Ls * Ks * specular_factor; // final specular intensity

	// emission
	vec3 texel_e = texture (texture_emission, TexCoords).rgb;

	outputColor = vec4 (Id + Is + Ia + texel_e, 1.0);

	//vec4 texColor = texture(texture_diffuse1, TexCoords); //mix(texture(texture_diffuse1, TexCoords), texture(texture_diffuse2, TexCoords),1);
	//vec4 texColor2 = texture(texture_diffuse2, TexCoords);
	//vec4 texColor3 = texture(texture_diffuse3, TexCoords);

	//vec4 mixedColor = texColor*color;
	
	//outputColor = texColor;
}
