# Graphics Architecture Activities - 2015/2 #

* Course: Graphics Architecture
* Local: Unisinos University  - C02 - 305
* Student: Tobias Beise Ulrich
* Professor: Luiz Gonzaga Jr
* Email: tobiasbulrich@gmail.com

### About ###


This repository includes all activities realized in the semester 2015/2 for Graphics Architecture couse based in the Anton's OpenGL 4 Tutorials - Antongerdelan.net.

### Grade A ###

* Activity 1: First usage of glfw, glm and glew;
* Activity 2: Cartoon Shading;
* Activity 3: 3D model and light shading;
* Activity 4: Lighting mapping;
* Activity 5: Normal mapping;

### Grade B ###

Final activity: each student receive a challenge. My challenge is implementation of Night Vision shader, using framebuffers.

Include a article about it.