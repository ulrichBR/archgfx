cmake_minimum_required(VERSION 3.0)
project (ArqGfxProject1)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")


## user defined paths
message("Source Dir: ${PROJECT_SOURCE_DIR}")
message("Destiny Dir: ${CMAKE_BINARY_DIR}")
#SET(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR})

# to add user-defined include path
include_directories(
	../../common/include
)
set(CMAKE_PREFIX_PATH "../../common")
# set(CMAKE_OSX_ARCHITECTURES i386)

if (NOT WIN32)
set(CMAKE_MODULE_PATH /usr/local/lib/cmake /usr/local/lib/x86_64-linux-gnu/cmake)
set(CMAKE_PREFIX_PATH /usr/local/lib/cmake/glfw)
endif (NOT WIN32)
 
# to add user-defined library path
if (WIN32)
    link_directories(../../common/msvc110)
    set(CMAKE_LIBRARY_PATH "../../common/msvc110")
endif (WIN32)

if (APPLE)  
   link_directories(../../common/osx_64)
   set(CMAKE_LIBRARY_PATH "../../common/osx_64")
endif (APPLE)


## required packages
find_package(OpenGL REQUIRED)
if(NOT OPENGL_FOUND)
    message("ERROR: OpenGL not found")
endif(NOT OPENGL_FOUND)

find_package(GLEW REQUIRED)
if(NOT GLEW_FOUND)
    message("ERROR: GLEW not found")
endif(NOT GLEW_FOUND)


if (APPLE)
find_package(PkgConfig REQUIRED)
pkg_search_module(GLFW REQUIRED glfw3)
if(NOT GLFW_FOUND)
    message("ERROR: GLFW3 not found")
endif(NOT GLFW_FOUND)
endif (APPLE)


file(GLOB ArqGfx_SRC
    "*.h"
    "*.cpp"
	"*.glsl"
)

file(GLOB ArqGfx_GLSL
	"*.glsl"
)

file(COPY ${ArqGfx_GLSL} DESTINATION ${CMAKE_BINARY_DIR})


add_executable(ArqGfx ${ArqGfx_SRC})

if (WIN32)
add_custom_command(TARGET ArqGfx POST_BUILD        # Adds a post-build event to MyTest
    COMMAND ${CMAKE_COMMAND} -E copy_directory  # which executes "cmake - E copy_if_different..."
        "${PROJECT_SOURCE_DIR}/../../common/DLLs"      # <--this is in-file
        $<TARGET_FILE_DIR:ArqGfx>)                 # <--this is out-file path
endif (WIN32)	
           # <--this is out-file path



target_link_libraries(ArqGfx  
${OPENGL_LIBRARY} 
${GLEW_LIBRARIES} 
glfw3dll 
${GLFW_LIBRARIES}
)
