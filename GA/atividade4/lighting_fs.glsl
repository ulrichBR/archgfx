

#version 330 core
struct Material {
    sampler2D diffuse;
    sampler2D specular;
	sampler2D ambient;
	sampler2D emission;
    float     shininess;
};  

struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 FragPos;  
in vec3 Normal;  
in vec2 TexCoords;
  
out vec4 color;
  
uniform vec3 viewPos;
uniform Material material;
uniform Light light;

void main()
{

	vec3 light_pos_eye = (viewPos * vec4 (light.position, 1.0)).xyz;

	// ambient
	vec3 Ka = texture (material.ambient, TexCoords).rgb;
	vec3 Ia = light.ambient * Ka;

	// difusse
	vec4 texel = texture (material.diffuse, TexCoords);
	vec3 Kd = texel.rgb;
	vec3 norm = normalize(Normal);
	vec3 surface_to_light_eye = normalize (viewPos - FragPos);
	float dp = max (0.0, dot (norm, surface_to_light_eye));
	vec3 Id = Kd * light.diffuse * dp; 

	// specular
	vec3 Ks = texture (material.specular, TexCoords).rgb;
	vec3 surface_to_viewer_eye =  normalize(viewPos - FragPos);
	vec3 half_way_eye = normalize (surface_to_viewer_eye + surface_to_light_eye);
	float dot_prod_specular = max (dot (half_way_eye, norm), 0.0);
	float specular_factor = pow (dot_prod_specular, material.shininess);
	vec3 Is = light.specular * Ks * specular_factor; // final specular intensity

	// emission
	vec3 texel_e = texture (material.emission, TexCoords).rgb;

	color = vec4 (Id + Is + Ia + texel_e, 1.0);

    // Ambient
    /*vec3 ambient = light.ambient * vec3(texture(material.diffuse, TexCoords));
  	
    // Diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));  
    
    // Specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
        
    color = vec4(ambient + diffuse + specular, 1.0f);  */
} 

